use sqlx::PgPool;
use glam::*;
use uuid::Uuid;
use super::base::{MIGRATOR, VoxelDb};
use crate::blocks::*;
use crate::util::octree::Octree;
use super::chunk::*;
use super::voxel::*;

pub struct PostgresDb {
	db: PgPool,
}

impl PostgresDb {
	pub async fn new(url: &str) -> Result<PostgresDb, String> {
		// create a db pool connection
		let db = PgPool::connect(url).await
			.map_err(|e| format!("{e}"))?;

		// run any new migrations on the db
		MIGRATOR.run(&db).await
			.map_err(|e| format!("{e}"))?;

		Ok(PostgresDb {
			db: db,
		})
	}
}

#[async_trait::async_trait]
impl VoxelDb for PostgresDb {
	async fn query_voxel_by_pos(&self, pos: I64Vec3) -> Option<Voxel> {
		/*sqlx::query_as!(
			Voxel, "SELECT * FROM voxels WHERE x=$1 AND y=$2 AND z=$3",
			pos.x, pos.y, pos.z,
		)
			.fetch_one(&self.db)
			.await
			.ok()*/
		None
	}

	async fn query_voxel(&self, uuid: Uuid) -> Option<Voxel> {
		/*sqlx::query_as!(Voxel, "SELECT * FROM voxels WHERE uuid=$1", uuid)
			.fetch_one(&self.db)
			.await
			.ok()*/
		None
	}

	async fn insert_voxel(&self, voxel: &Voxel) -> Result<(), String> {
		/*sqlx::query!(
			"INSERT INTO voxels (uuid, x, y, z, material) VALUES ($1, $2, $3, $4, $5)",
			voxel.uuid, voxel.x, voxel.y, voxel.z, voxel.material,
		)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;*/

		Ok(())
	}

	async fn update_voxel(&self, voxel: &Voxel) -> Result<(), String> {
		/*sqlx::query!(
			"UPDATE voxels SET x=$2, y=$3, z=$4, material=$5 WHERE uuid=$1",
			voxel.uuid, voxel.x, voxel.y, voxel.z, voxel.material,
		)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;*/

		Ok(())
	}

	async fn delete_voxel(&self, uuid: Uuid) -> Result<(), String> {
		sqlx::query!("DELETE FROM voxels WHERE uuid=$1", uuid)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;

		Ok(())
	}
	async fn delete_voxel_by_pos(&self, pos: I64Vec3) -> Result<(), String> {
		sqlx::query!("DELETE FROM voxels WHERE x=$1 AND y=$2 AND z=$3", pos.x, pos.y, pos.z)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;

		Ok(())
	}

	async fn query_chunk(&self, pos: IVec3) -> Option<Chunk> {
		sqlx::query_as!(Chunk, "SELECT * FROM chunks WHERE x=$1 AND y=$2 AND z=$3", pos.x, pos.y, pos.z)
			.fetch_one(&self.db)
			.await
			.ok()
	}

	async fn query_chunk_voxels(&self, pos: IVec3) -> Option<LoadedChunk> {
		// first check that the chunk exists
		let chunk = match self.query_chunk(pos).await {
			Some(c) => c,
			None => return None,
		};

		// find the min/max voxel coords in the chunk
		let min = pos_to_absolute(UVec3::splat(0), pos);
		let max = pos_to_absolute(UVec3::splat(crate::constants::CHUNK_SIZE), pos);

		// query all voxels in the db within those coordinates
		/*let rows = sqlx::query_as!(
			Voxel,
			"SELECT * FROM voxels WHERE x >= $1 AND x < $2 AND y >= $3 AND y < $4 AND z >= $5 AND z < $6",
			min.x, max.x,
			min.y, max.y,
			min.z, max.z,
		)
		 	.fetch_all(&self.db)
		 	.await
			.map_err(|e| eprintln!("query_chunk_voxels: {e}"))
		 	.ok()
			.unwrap_or(vec![]);*/

		// construct the obtained chunk data
		let mut loaded_chunk = LoadedChunk::new(pos);
		loaded_chunk.chunk = chunk;

		/*for record in rows {
			loaded_chunk.set_voxel(record)
				.map_err(|e| eprintln!("set_voxel: {e}"))
				.ok();
		}*/

		Some(loaded_chunk)
	}

	async fn insert_chunk_voxels(&self, chunk: &Chunk, voxels: &Octree<i64>) -> Result<(), String> {
		let voxel_data = voxels.iter_dfs_values()
			.map(|(pos, material)| (pos_to_absolute(pos, chunk.pos()), *material))
			.collect::<Vec<_>>();

		let x = voxel_data.iter().map(|v| v.0.x).collect::<Vec<_>>();
		let y = voxel_data.iter().map(|v| v.0.y).collect::<Vec<_>>();
		let z = voxel_data.iter().map(|v| v.0.z).collect::<Vec<_>>();
		let material = voxel_data.iter().map(|v| v.1).collect::<Vec<_>>();

		// batch insert all chunks
		sqlx::query!(
			"INSERT INTO voxels (x, y, z, material) SELECT * FROM UNNEST($1::bigint[], $2::bigint[], $3::bigint[], $4::bigint[])",
			&x, &y, &z, &material,
		)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;

		// if all voxels insert successfully, insert the chunk record
		let chunk_pos = chunk.pos();
		sqlx::query!(
			"INSERT INTO chunks (x, y, z) VALUES ($1, $2, $3)",
			chunk_pos.x, chunk_pos.y, chunk_pos.z,
		)
			.fetch_all(&self.db)
			.await
			.map_err(|e| format!("{e}"))?;

		Ok(())
	}
}


