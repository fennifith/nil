use glam::*;
use sqlx::postgres::PgRow;
use sqlx::Row;
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct Voxel {
	pub uuid: Uuid,
	pub x: i64,
	pub y: i64,
	pub z: i64,
	pub material: u8,
	pub created_at: chrono::DateTime<chrono::Utc>,
	pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl std::default::Default for Voxel {
	fn default() -> Self {
		Voxel {
			uuid: Uuid::new_v4(),
			x: 0,
			y: 0,
			z: 0,
			material: 0,
			created_at: chrono::Utc::now(),
			updated_at: chrono::Utc::now(),
		}
	}
}

impl <'r> sqlx::FromRow<'r, PgRow> for Voxel {
	fn from_row(row: &'r PgRow) -> Result<Self, sqlx::Error> {
		Ok(Self {
			uuid: row.try_get("uuid")?,
			x: row.try_get("x")?,
			y: row.try_get("y")?,
			z: row.try_get("z")?,
			material: row.try_get::<i32, _>("material")? as u8,
			created_at: row.try_get("created_at")?,
			updated_at: row.try_get("updated_at")?,
			..Voxel::default()
		})
	}
}

impl PartialEq for Voxel {
	fn eq(&self, other: &Self) -> bool {
		self.uuid.eq(&other.uuid)
			&& self.pos().eq(&other.pos())
			&& self.material == other.material
	}
}

impl Voxel {
	pub fn new(pos: I64Vec3, material: u8) -> Self {
		Self {
			x: pos.x,
			y: pos.y,
			z: pos.z,
			material,
			..Voxel::default()
		}
	}

	pub fn pos(&self) -> I64Vec3 {
		I64Vec3::new(self.x, self.y, self.z)
	}

	pub fn chunk(&self) -> IVec3 {
		crate::blocks::chunk::pos_to_chunk(self.pos())
	}

	pub fn move_to(&mut self, pos: &I64Vec3) {
		self.x = pos.x;
		self.y = pos.y;
		self.z = pos.z;
	}
}
