use glam::*;
use sqlx::FromRow;

#[derive(Clone, FromRow, Debug)]
pub struct Chunk {
	pub x: i32,
	pub y: i32,
	pub z: i32,
}

impl std::default::Default for Chunk {
	fn default() -> Self {
		Chunk {
			x: 0,
			y: 0,
			z: 0,
		}
	}
}

impl Chunk {
	pub fn new(pos: IVec3) -> Self {
		Self {
			x: pos.x,
			y: pos.y,
			z: pos.z,
		}
	}

	pub fn pos(&self) -> IVec3 {
		IVec3::new(self.x, self.y, self.z)
	}
}
