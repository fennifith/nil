use glam::*;
use uuid::Uuid;
use crate::blocks::chunk::LoadedChunk;
use crate::util::octree::Octree;

use super::voxel::*;
use super::chunk::*;

pub static MIGRATOR: sqlx::migrate::Migrator = sqlx::migrate!();

#[async_trait::async_trait]
pub trait VoxelDb {
	async fn query_voxel_by_pos(&self, pos: I64Vec3) -> Option<Voxel>;
	async fn query_voxel(&self, uuid: Uuid) -> Option<Voxel>;
	async fn update_voxel(&self, voxel: &Voxel) -> Result<(), String>;
	async fn insert_voxel(&self, voxel: &Voxel) -> Result<(), String>;
	async fn delete_voxel(&self, uuid: Uuid) -> Result<(), String>;
	async fn delete_voxel_by_pos(&self, pos: I64Vec3) -> Result<(), String>;

	async fn query_chunk(&self, pos: IVec3) -> Option<Chunk>;

	async fn query_chunk_voxels(&self, pos: IVec3) -> Option<LoadedChunk>;
	async fn insert_chunk_voxels(&self, chunk: &Chunk, voxels: &Octree<i64>) -> Result<(), String>;
}
