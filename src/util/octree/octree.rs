use glam::*;
use std::ops::*;

#[derive(Clone)]
pub struct Octree<T> {
	// height should be log2 of CHUNK_SIZE
	pub height: usize,
	pub root: OctreeNode<T>,
}

pub type OctreeBranches<T> = [OctreeNode<T>; 8];
const BRANCH_LLL: usize = 0;
const BRANCH_LLH: usize = 1;
const BRANCH_LHL: usize = 2;
const BRANCH_LHH: usize = 3;
const BRANCH_HLL: usize = 4;
const BRANCH_HLH: usize = 5;
const BRANCH_HHL: usize = 6;
const BRANCH_HHH: usize = 7;

#[derive(Clone)]
pub enum OctreeNode<T> {
	Leaf(T),
	Branch(Box<OctreeBranches<T>>),
	Empty,
}

pub struct OctreeCell<'a, T> {
	pub height: usize,
	pub index: usize,
	pub node: &'a OctreeNode<T>,
}

impl <'a, T> OctreeCell<'a, T> {
	pub fn pos(&self) -> UVec3 {
		OctreeNode::<bool>::get_branch_coord(self.index, self.height)
	}

	pub fn length(&self) -> u32 {
		2u32.pow(self.height as u32)
	}
}

impl <T> OctreeNode<T> {
	fn new_branches() -> OctreeBranches<T> {
		[
			OctreeNode::Empty, OctreeNode::Empty, OctreeNode::Empty, OctreeNode::Empty,
			OctreeNode::Empty, OctreeNode::Empty, OctreeNode::Empty, OctreeNode::Empty,
		]
	}

	pub fn new_branch() -> Self {
		OctreeNode::Branch(Box::new(OctreeNode::new_branches()))
	}

	/**
	 * At the given tree height, find the array index (0-7) of the provided
	 * coordinate.
	 *
	 * height: the current height in the tree;
	 * - height 0 expects only leaf nodes, so it is a 1:1 conversion from xyz -> index
	 * - height 1 expects branch nodes, so 0,0,0 and 0,0,1 both point to index 0
	 */
	pub fn get_branch_index(x: u32, y: u32, z: u32, height: usize) -> usize {
		let branch = x.shr(height).bitand(1).shl(2)
			| y.shr(height).bitand(1).shl(1)
			| z.shr(height).bitand(1);

		branch as usize
	}

	/**
	 * Converts a branch back into its respective coordinates
	 *
	 * These can be added together at each level of the tree to
	 * find the exact coordinate of the voxel
	 */
	pub fn get_branch_coord(branch: usize, height: usize) -> UVec3 {
		let x = (branch as u32).shr(2u32).bitand(1).shl(height);
		let y = (branch as u32).shr(1u32).bitand(1).shl(height);
		let z = (branch as u32).bitand(1).shl(height);
		UVec3::new(x, y, z)
	}

	/**
	 * Traverse down the height of the tree until a node at the given coords is found.
	 */
	pub fn get<'a>(&'a self, x: u32, y: u32, z: u32, mut height: usize) -> OctreeCell<'a, T> {
		let mut node = self;

		while height > 0 {
			height = height - 1;
			let index = Self::get_branch_index(x, y, z, height);

			node = match node {
				OctreeNode::Branch(b) => &b[index],
				_ => {
					return OctreeCell {
						height,
						index,
						node: &OctreeNode::Empty,
					};
				}
			};
		}

		OctreeCell {
			height,
			index: Self::get_branch_index(x, y, z, height),
			node,
		}
	}

	pub fn get_index<'a>(&'a self, index: usize) -> &'a OctreeNode<T> {
		if let OctreeNode::Branch(b) = self {
			&b[index]
		} else {
			&OctreeNode::Empty
		}
	}

	pub fn get_index_iter<'a, I: std::iter::Iterator<Item = usize>>(&'a self, indeces: I) -> &'a OctreeNode<T> {
		indeces.fold(self, |acc, e| acc.get_index(e))
	}

	/**
	 * Insert a new node at a specific position in the tree (adding new branches as necessary)
	 */
	pub fn insert(&mut self, x: u32, y: u32, z: u32, mut height: usize, value: T) {
		let mut root = self;

		while height > 0 {
			height = height - 1;

			let branches = match root {
				OctreeNode::Branch(b) => b,
				_ => {
					*root = OctreeNode::new_branch();

					if let OctreeNode::Branch(b) = root {
						b
					} else {
						panic!("Could not perform insert: octree is broken")
					}
				}
			};

			let index = Self::get_branch_index(x, y, z, height);
			root = &mut branches[index];
		}

		*root = OctreeNode::Leaf(value);
	}

	pub fn remove(&mut self, x: u32, y: u32, z: u32, mut height: usize) -> OctreeNode<T> {
		let mut root = self;
		while height > 0 {
			height = height - 1;

			let branches = match root {
				OctreeNode::Branch(b) => b,
				_ => return OctreeNode::Empty,
			};

			let index = Self::get_branch_index(x, y, z, height);
			root = &mut branches[index];
		}

		std::mem::replace(root, OctreeNode::Empty)
	}

	pub fn is_leaf(&self) -> bool {
		matches!(self, OctreeNode::Leaf(_))
	}

	pub fn is_branch(&self) -> bool {
		matches!(self, OctreeNode::Branch(_))
	}

	pub fn is_empty(&self) -> bool {
		matches!(self, OctreeNode::Empty)
	}

	pub fn size(&self) -> u32 {
		match self {
			OctreeNode::Branch(b) => {
				// count non-empty branches from the current node
				b.iter()
					.filter(|n| !n.is_empty())
					.count() as u32
			},
			OctreeNode::Leaf(_) => 1,
			OctreeNode::Empty => 0,
		}
	}
}

impl <T> std::fmt::Debug for OctreeNode<T> where T: std::fmt::Debug {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			OctreeNode::Branch(b) => {
				for (i, branch) in b.iter().enumerate() {
					if branch.is_empty() { continue; }

					let branch_str = format!("\n{:?}", branch)
						.replace("\n", &format!("\n[{i}] "));

					f.write_str(&branch_str)?;
				}
				f.write_str("")
			},
			OctreeNode::Leaf(v) => f.write_str(&format!("-> {:?}", v)),
			OctreeNode::Empty => f.write_str("(empty)"),
		}
	}
}

impl <T> std::fmt::Debug for Octree<T> where T: std::fmt::Debug {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.root.fmt(f)
	}
}

impl <T> Octree<T> {
	pub fn new(height: usize) -> Self {
		Self {
			height,
			root: OctreeNode::Empty,
		}
	}

	/**
	 * Initialize an octree with a height that
	 * can fit the given length on all axes
	 */
	pub fn with_length(length: usize) -> Self {
		let height = (length as f32).log2().ceil() as usize;
		Self::new(height)
	}

	pub fn get<'a>(&'a self, index: UVec3) -> Option<&'a T> {
		let result = self.get_cell(index);

		if let OctreeNode::Leaf(v) = result.node {
			Some(&v)
		} else {
			None
		}
	}

	pub fn get_cell<'a>(&'a self, index: UVec3) -> OctreeCell<'a, T> {
		self.root.get(index.x, index.y, index.z, self.height)
	}

	pub fn insert(&mut self, index: UVec3, value: T) {
		self.root.insert(index.x, index.y, index.z, self.height, value);
	}

	pub fn remove(&mut self, index: UVec3) -> Option<T> {
		let result = self.root.remove(index.x, index.y, index.z, self.height);
		if let OctreeNode::Leaf(v) = result {
			Some(v)
		} else {
			None
		}
	}

	/**
	 * Returns the max value of the octree in any direction,
	 * given its height
	 */
	pub fn length(&self) -> u32 {
		2u32.pow(self.height as u32)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn octree_get_some() {
		let mut octree: Octree<bool> = Octree::new(3);
		octree.insert(UVec3::new(0, 2, 3), true);

		assert_eq!(
			octree.get(UVec3::new(0, 2, 3)),
			Some(&true)
		);
	}

	#[test]
	fn octree_get_none() {
		let mut octree: Octree<bool> = Octree::new(3);
		octree.insert(UVec3::new(0, 2, 3), true);

		assert_eq!(
			octree.get(UVec3::new(0, 2, 4)),
			None
		);
	}

	#[test]
	fn octree_remove() {
		let mut octree: Octree<bool> = Octree::new(3);
		octree.insert(UVec3::new(0, 3, 0), true);

		assert_eq!(
			octree.remove(UVec3::new(0, 3, 0)),
			Some(true)
		);

		assert_eq!(
			octree.get(UVec3::new(0, 3, 0)),
			None
		);
	}

	#[test]
	fn octree_max_index() {
		let octree: Octree<bool> = Octree::new(3);
		let index = octree.length();
		assert_eq!(index, 8);
	}

	#[test]
	fn octree_with_length() {
		let octree: Octree<bool> = Octree::with_length(8);
		assert_eq!(octree.height, 3);
	}

	#[test]
	fn node_get_branch_coord() {
		let (x, y, z) = (3, 4, 5);
		let stack = vec![
			OctreeNode::<bool>::get_branch_index(x, y, z, 2),
			OctreeNode::<bool>::get_branch_index(x, y, z, 1),
			OctreeNode::<bool>::get_branch_index(x, y, z, 0),
		];

		let coord = OctreeNode::<bool>::get_branch_coord(stack[0], 2)
			+ OctreeNode::<bool>::get_branch_coord(stack[1], 1)
			+ OctreeNode::<bool>::get_branch_coord(stack[2], 0);

		assert_eq!(coord, UVec3::new(x, y, z));
	}
}

