use std::{iter::*, collections::VecDeque};
use glam::*;
use super::octree::*;

pub struct OctreeDfs<'a, T> {
	octree: &'a Octree<T>,
	stack: VecDeque<usize>,
	is_done: bool,
}

impl <T> Octree<T> {
	pub fn iter_dfs<'a>(&'a self) -> OctreeDfs<'a, T> {
		OctreeDfs {
			octree: &self,
			stack: VecDeque::new(),
			is_done: false,
		}
	}

	pub fn iter_dfs_values<'a>(&'a self) -> impl Iterator<Item = (UVec3, &'a T)> {
		self.iter_dfs()
			.filter(|(_, _, node)| node.is_leaf())
			.filter_map(|(pos, _, node)| {
				if let OctreeNode::Leaf(v) = node {
					Some((pos, v))
				} else {
					None
				}
			})
	}
}

impl <'a, T> Iterator for OctreeDfs<'a, T> {
	type Item = (UVec3, usize, &'a OctreeNode<T>);

	fn next(&mut self) -> Option<Self::Item> {
		if self.is_done {
			return None;
		}

		// get the current node from the stack vec
		let node = self.octree.root.get_index_iter(self.stack.iter().cloned());
		let node_height = self.octree.height - self.stack.len();
		let node_pos: UVec3 = self.stack.iter()
			.enumerate()
			.map(|(i, idx)| OctreeNode::<bool>::get_branch_coord(*idx, self.octree.height - i - 1))
			.sum();

		// if the node is a branch, proceed into it on the next iter
		if node.is_branch() {
			self.stack.push_back(0);
		} else {
			// otherwise, traverse upwards until we find a branch that isn't fully iterated
			loop {
				let last_index = match self.stack.pop_back() {
					Some(i) => i,
					_ => {
						self.is_done = true;
						0
					},
				};

				// if this branch has more nodes, increment it for the next iter
				if last_index < 7 {
					self.stack.push_back(last_index + 1);
					break;
				}
			}
		}

		Some((node_pos, node_height, &node))
	}
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn iter_empty() {
		let octree: Octree<bool> = Octree::new(3);
		let iter = octree.iter_dfs().collect::<Vec<_>>();

		assert_eq!(iter.len(), 1);
		assert_eq!(iter[0].0, UVec3::new(0, 0, 0));
		assert_eq!(iter[0].1, 3);
	}

	#[test]
	fn iter_some() {
		let mut octree: Octree<bool> = Octree::new(3);
		octree.insert(UVec3::new(0, 0, 0), true);
		octree.insert(UVec3::new(0, 3, 6), true);

		let vecs: Vec<UVec3> = octree.iter_dfs()
			.filter(|(_, _, node)| node.is_leaf())
			.map(|(pos, _, _)| pos)
			.collect::<Vec<_>>();

		assert_eq!(vecs.len(), 2);
		assert_eq!(vecs[0], UVec3::new(0, 0, 0));
		assert_eq!(vecs[1], UVec3::new(0, 3, 6));
	}
}
