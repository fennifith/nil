use std::iter::*;
use glam::*;
use super::octree::*;
use crate::util::ray_traverse::*;

#[derive(Debug)]
pub struct OctreeRaycast<'a, T> {
	octree: &'a Octree<T>,
	ray: RayTraverseIter,
	last_pos: UVec3,
}

impl <T> Octree<T> {
	pub fn iter_raycast_cells<'a>(&'a self, from: UVec3, to: UVec3) -> OctreeRaycast<'a, T> {
		let ray = ray_traverse(from.as_vec3a(), to.as_vec3a());

		OctreeRaycast {
			octree: &self,
			ray,
			last_pos: from,
		}
	}

	pub fn iter_raycast<'a>(&'a self, from: UVec3, to: UVec3) -> impl Iterator<Item = (UVec3, &'a T)> {
		self.iter_raycast_cells(from, to)
			.filter_map(|cell| {
				if let OctreeNode::Leaf(v) = cell.node {
					Some((cell.pos(), v))
				} else {
					None
				}
			})
	}
}

impl <'a, T> Iterator for OctreeRaycast<'a, T> {
	type Item = OctreeCell<'a, T>;

	fn next(&mut self) -> Option<Self::Item> {
		// this is *not* the most efficient implementation for this...
		// but I kinda just want it to work

		// ... I'll rewrite this function again later, probably

		loop {
			// get the next position along the ray
			let query_pos = match self.ray.next() {
				Some(p) => p,
				_ => return None,
			};

			// check that the point is within bounds of the octree
			if query_pos.min_element() < 0 || query_pos.max_element() >= self.octree.length() as i32 {
				continue;
			}

			let result = self.octree.get_cell(query_pos.as_uvec3());

			if result.pos() != self.last_pos {
				self.last_pos = result.pos();
				return Some(result);
			}
		}
	}
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn iter_empty() {
		let octree: Octree<bool> = Octree::new(3);
		let iter = octree.iter_raycast_cells(UVec3::splat(0), UVec3::splat(16)).collect::<Vec<_>>();

		// there should only be one set of branches in this octree, so the last returned position should be (4,4,4)
		let last = iter.last().unwrap();
		assert_eq!(last.pos(), UVec3::splat(4));
	}
}
