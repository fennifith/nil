use std::ops::Shl;

use super::octree::*;
use crate::util::traits::*;

impl <T> ToBytes for Octree<T> where T: ToBytes {
	fn write_bytes(&self, vec: &mut Vec<u8>) {
		for (_, _, branch) in self.iter_dfs() {
			match branch {
				OctreeNode::Branch(b) => {
					// construct a bitmask to indicate which branches are present
					let bitmask: u8 = b.iter()
						.enumerate()
						.filter(|(_, b)| !b.is_empty())
						.fold(0u8, |acc, (i, _)| acc | 1u8.shl(7 - i));

					vec.push(bitmask);
				},
				OctreeNode::Leaf(v) => v.write_bytes(vec),
				OctreeNode::Empty => (),
			}
		}
	}
}
