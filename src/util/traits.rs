pub trait ToBytes {
	fn write_bytes(&self, vec: &mut Vec<u8>);

	fn to_bytes(&self) -> Vec<u8> {
		let mut bytes = Vec::new();
		self.write_bytes(&mut bytes);
		bytes
	}
}
