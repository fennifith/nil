use glam::*;
use std::iter::*;

#[derive(Debug)]
pub struct RayTraverseIter {
	point: Vec3A,
	dir_sign: Vec3A,
	delta: Vec3A,
	t_max: Vec3A,
}

/**
 * Create an iterator through all grid points within the
 * provided ray.
 */
pub fn ray_traverse(from: Vec3A, to: Vec3A) -> RayTraverseIter {
	let dir = to - from;
	let point = from.floor();
	let dir_sign = dir.signum();
	let delta = (dir_sign / dir).min(Vec3A::splat(1.));
	let t_max = ((point + dir_sign.max(Vec3A::splat(0.)) - from) / dir).abs();

	RayTraverseIter {
		point,
		dir_sign,
		delta,
		t_max,
	}
}

impl Iterator for RayTraverseIter {
	type Item = IVec3;

	fn next(&mut self) -> Option<Self::Item> {
		// hacky correction for negative values; need to subtract 1 delta from any negative axes
		// when comparing whether the ray has reached the "to" point
		let t_next = self.t_max - (self.delta * self.dir_sign.min(Vec3A::splat(0.)));
		if t_next.min_element() > 1. {
			return None;
		}

		// find the minimum axis along t; i.e. the closest boundary to the edge of a voxel
		let cmp = if self.t_max.x < self.t_max.y && self.t_max.x < self.t_max.z {
			Vec3A::X
		} else if self.t_max.y < self.t_max.z {
			Vec3A::Y
		} else {
			Vec3A::Z
		};

		// increment the point and t_max along the chosen edge
		self.t_max = self.delta.mul_add(cmp, self.t_max);
		self.point = self.dir_sign.mul_add(cmp, self.point);

		Some(self.point.as_ivec3())
	}
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn traverse_0_to_13() {
		let traversal = ray_traverse(Vec3A::splat(0.), Vec3A::splat(13.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(0, 0, 1));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(13));
	}

	#[test]
	fn traverse_0_to_m13() {
		let traversal = ray_traverse(Vec3A::splat(0.), Vec3A::splat(-13.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(0, 0, -1));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(-13));
	}

	#[test]
	fn traverse_2_to_4() {
		let traversal = ray_traverse(Vec3A::splat(2.), Vec3A::splat(4.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(2, 2, 3));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(4));
	}

	#[test]
	fn traverse_m2_to_m4() {
		let traversal = ray_traverse(Vec3A::splat(-2.), Vec3A::splat(-4.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(-2, -2, -3));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(-4));
	}

	#[test]
	fn traverse_4_to_2() {
		let traversal = ray_traverse(Vec3A::splat(4.), Vec3A::splat(2.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(4, 4, 3));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(2));
	}

	#[test]
	fn traverse_4_to_m2() {
		let traversal = ray_traverse(Vec3A::splat(4.), Vec3A::splat(-2.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(4, 4, 3));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(-2));
	}

	#[test]
	fn traverse_m4_to_2() {
		let traversal = ray_traverse(Vec3A::splat(-4.), Vec3A::splat(2.))
			.collect::<Vec<_>>();

		assert_eq!(*traversal.first().unwrap(), IVec3::new(-4, -4, -3));
		assert_eq!(*traversal.last().unwrap(), IVec3::splat(2));
	}

	#[test]
	fn traverse_y_2_to_1() {
		let traversal = ray_traverse(Vec3A::new(1., 2.2, 1.), Vec3A::new(1., 1., 1.))
			.collect::<Vec<_>>();

		assert_eq!(traversal.len(), 1);
		assert_eq!(*traversal.first().unwrap(), IVec3::new(1, 1, 1));
	}

	#[test]
	fn traverse_y2_to_y1() {
		let traversal = ray_traverse(Vec3A::new(1., 2.01, 1.), Vec3A::new(1., 1.99, 1.))
			.collect::<Vec<_>>();

		assert_eq!(traversal.len(), 1);
		assert_eq!(*traversal.first().unwrap(), IVec3::new(1, 1, 1));
	}
}
