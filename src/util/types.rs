use lazy_static::lazy_static;
use glam::*;

#[derive(Clone, Debug)]
pub struct CubeSide(pub usize);

lazy_static! {
	static ref FACE_MAP: &'static [&'static str] = &[
		"left",
		"top",
		"front",
		"right",
		"bottom",
		"back",
	];
}

impl CubeSide {
	pub fn iter() -> impl Iterator<Item = CubeSide> {
		[0; 6].iter()
			.enumerate()
			.map(|(i, _)| CubeSide(i))
	}

	pub fn from_str(string: &str) -> Option<CubeSide> {
		if string.len() == 0 { return None }
		FACE_MAP.iter().position(|&s| string.ends_with(s)).map(|f| CubeSide(f))
	}

	pub fn from_ivec3(direction: IVec3) -> CubeSide {
		let i =
			if direction.z > 0 { 2 } else if direction.z < 0 { 5 }
			else if direction.y > 0 { 1 } else if direction.y < 0 { 4 }
			else if direction.x > 0 { 3 } else { 0 };

		CubeSide(i)
	}

	pub fn from_vec3a(direction: Vec3A) -> CubeSide {
		let vec = IVec3::new(
			if direction.x > 0.0 { 1 } else if direction.x < 0.0 { -1 } else { 0 },
			if direction.y > 0.0 { 1 } else if direction.y < 0.0 { -1 } else { 0 },
			if direction.z > 0.0 { 1 } else if direction.z < 0.0 { -1 } else { 0 },
		);

		Self::from_ivec3(vec)
	}

	pub fn from_vec3a_axis(direction: Vec3A) -> CubeSide {
		let direction_max = direction.abs().max_element();

		let mask = Vec3A::select(
			direction.abs().cmpge(Vec3A::splat(direction_max)),
			direction,
			Vec3A::splat(0.0),
		);

		Self::from_vec3a(mask)
	}

	pub fn to_quat(&self) -> Quat {
		return Quat::from_rotation_arc(
			Vec3::Z,
			self.to_vec().as_vec3().normalize(),
		);
	}

	pub fn to_index(&self) -> usize {
		self.0
	}

	pub fn to_string(&self) -> String {
		FACE_MAP[self.0].to_string()
	}

	pub fn to_vec(&self) -> IVec3 {
		return match self.0 {
			0 => IVec3::new(-1, 0, 0),
			1 => IVec3::new(0, 1, 0),
			2 => IVec3::new(0, 0, 1),
			3 => IVec3::new(1, 0, 0),
			4 => IVec3::new(0, -1, 0),
			5 => IVec3::new(0, 0, -1),
			_ => {
				let mut vec = IVec3::splat(0);

				let str = self.to_string();
				if !str.contains("-") {
					panic!("Unexpected face number: {} = {}", self.0, str);
				}

				for part in self.to_string().split("-") {
					vec += CubeSide::from_str(part).unwrap().to_vec()
				}

				vec
			}
		};
	}

	pub fn opposite(&self) -> CubeSide {
		CubeSide((self.0 + 3) % 6)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn cube_side() {
		let top = IVec3::new(0, 1, 0);
		let top2 = CubeSide::from_str("top").unwrap().to_vec();

		assert_eq!(top, top2);
	}

	#[test]
	fn cube_side_opposite() {
		let top = IVec3::new(0, 1, 0);
		let top2 = CubeSide::from_str("bottom").unwrap().opposite().to_vec();

		assert_eq!(top, top2);
	}

	#[test]
	fn cube_side_convert() {
		for face_str in FACE_MAP.iter() {
			let v = CubeSide::from_str(face_str).unwrap().to_vec();
			let v2 = CubeSide::from_ivec3(v).to_vec();

			assert_eq!(v, v2);
		}
	}
}
