use std::collections::{HashMap, LinkedList};
use std::cmp::Eq;
use std::hash::Hash;

#[derive(Clone, Debug)]
pub struct Block<K> {
	pub offset: usize,
	pub size: usize,
	pub key: Option<K>,
}

impl <K> Block<K> {
	pub fn range(&self) -> std::ops::Range<u32> {
		self.offset as u32..(self.offset + self.size) as u32
	}
}

#[derive(Debug)]
pub struct Allocator<K> {
	unit: usize,
	blocks: LinkedList<Block<K>>,
	keys: HashMap<K, usize>,
	size: usize,
}

impl <K> Allocator<K> where K: Clone + Eq + Hash {

	pub fn new(unit: usize) -> Allocator<K> {
		Allocator {
			unit,
			blocks: LinkedList::new(),
			keys: HashMap::new(),
			size: 0,
		}
	}

	pub fn size(&self) -> usize {
		self.size * self.unit
	}

	pub fn blocks(&self) -> Vec<Block<K>> {
		self.blocks.iter()
			.filter(|b: &&Block<K>| b.key.is_some())
			.cloned()
			.collect()
	}

	pub fn allocate(&mut self, key: K, s: usize) -> usize {
		let size = s / self.unit;

		// search for existing blocks matching the key first...
		if let Some(offset) = self.keys.get(&key) {
			let (_, block) = self.blocks.iter_mut()
				.enumerate()
				.find(|(_i, b)| b.key.as_ref().map(|k| k.eq(&key)) == Some(true))
				.unwrap();

			// if a block exists, return it (only if the request size fits within it)
			if size <= block.size {
				block.size = size;
				return *offset * self.unit;
			} else {
				// if the size is not big enough, deallocate, and try to create a new one
				self.deallocate(key.clone()).ok();
			}
		}

		let block_res = self.blocks.iter()
			.enumerate()
			.find(|(_i, b)| b.key.is_none() && b.size >= size)
			.map(|(i, _b)| i);

		if let Some(i) = block_res {
			let mut blocks = self.blocks.split_off(i);
			let mut block = blocks.front_mut().unwrap();
			let offset = block.offset;

			// split block & return
			self.keys.insert(key.clone(), block.offset);
			self.blocks.push_back(Block {
				offset: block.offset,
				size,
				key: Some(key.clone()),
			});

			// reduce empty block size
			if block.size > size {
				block.offset += size;
				block.size -= size;
			} else {
				blocks.pop_front();
			}

			self.blocks.append(&mut blocks);
			return offset * self.unit;
		}

		// couldn't find a block; allocate on the end
		let offset = self.size;
		self.size += size;
		self.keys.insert(key.clone(), offset);
		self.blocks.push_back(Block {
			offset,
			size,
			key: Some(key.clone()),
		});

		offset * self.unit
	}

	pub fn deallocate(&mut self, key: K) -> Result<(), String> {
		self.keys.remove(&key);

		let i = self.blocks.iter()
			.enumerate()
			.find(|(_i, b)| b.key.as_ref().map(|k| k.eq(&key)) == Some(true))
			.map(|(i, _b)| i)
			.ok_or(format!("Block not in allocator"))?;

		// remove unused block
		let mut blocks = self.blocks.split_off(i);
		let mut block_removed = blocks.pop_front().unwrap();

		// merge neighboring empty blocks
		match blocks.front_mut().filter(|b| b.key.is_none()) {
			Some(block) => {
				// merge with block after
				block.offset = block_removed.offset;
				block.size += block_removed.size;
			},
			None => match self.blocks.back_mut().filter(|b| b.key.is_none()) {
				Some(block) => {
					// merge with block before
					block.size += block_removed.size;
				},
				None => {
					// replace with empty block
					block_removed.key = None;
					blocks.push_front(block_removed);
				},
			},
		}

		self.blocks.append(&mut blocks);

		// remove tailing empty blocks
		if Some(true) == self.blocks.back().map(|b| b.key.is_none()) {
			let block = self.blocks.pop_back().unwrap();
			self.size -= block.size;
		}

		Ok(())
	}

	pub fn clear(&mut self) {
		self.blocks.clear();
		self.keys.clear();
		self.size = 0;
	}

}

impl <K> std::iter::IntoIterator for Allocator<K> where K: Clone {
	type Item = (usize, Option<K>);
	type IntoIter =  std::vec::IntoIter<Self::Item>;
	fn into_iter(self) -> Self::IntoIter {
		let mut ret: Vec<(usize, Option<K>)> = Vec::new();

		for block in &self.blocks {
			ret.push((block.offset, block.key.clone()));
		}

		ret.into_iter()
	}
}

impl <K> std::string::ToString for Allocator<K> where K: ToString {
	fn to_string(&self) -> String {
		let mut ret = String::new();

		for block in &self.blocks {
			if let Some(key) = &block.key {
				ret.push_str(format!("[{}, {} + {}], ", key.to_string(), block.offset, block.size).as_str());
			} else {
				ret.push_str(format!("[None, {} + {}], ", block.offset, block.size).as_str());
			}
		}

		return ret.to_string();
	}
}
