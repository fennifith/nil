use glam::*;

use crate::{blocks::*, util::ray_traverse};
use crate::blocks::chunk_group::*;

use super::*;

const GRAVITY: f32 = 0.98;
const AIR_RESISTANCE: f32 = 1.2;

pub fn tick_physics(obj: &dyn PhysicsObject, chunks: &ChunkGroup<LoadedChunk>) -> PhysicsEvent {
	// TODO: raycast in obj.velocity distance to find collisions

	// TODO: handle multiple objects - combine multiple voxels into a single PhysicsObject impl?
	//   (PhysicsObject would need to specify object bounds, and raycast would need to check the entire area within bounds)

	// TODO: rewrite this (and ray traversal) using i64 numbers to avoid overflow errors

	// TODO: add updated_at behavior to multiply velocity over multiple ticks

	// if the object is stuck in a voxel, push it out?
	if chunks.get_voxel(obj.get_pos().as_i64vec3()).is_some() {
		println!("Stuck in an object!!! Bad physics! No!");
		return PhysicsEvent {
			pos: obj.get_pos() + Vec3A::new(0., 1., 0.),
			velocity: obj.get_velocity() / AIR_RESISTANCE,
			collision: None,
		};
	}

	let mut pos = obj.get_pos();
	let mut velocity = obj.get_velocity() / AIR_RESISTANCE;
	velocity.y -= GRAVITY;

	// max movement should be 1
	velocity = velocity.clamp(Vec3A::splat(-1.), Vec3A::splat(1.));

	// if let Some(v) = chunks.get_voxel((pos + velocity).as_i64vec3()) {
	// 	let diff = (v.pos().as_vec3a() - pos).abs();
	// 	if diff.x > diff.y && diff.x > diff.z {
	// 		velocity.x = 0.;
	// 	} else if diff.y > diff.z {
	// 		velocity.y = 0.;
	// 	} else {
	// 		velocity.z = 0.;
	// 	}
	// }

	// binary vector masks; 001, 010, 011, ... 111
	for i in 1..8usize {
		let mask = BVec3A::new(
			i & 0b100 != 0,
			i & 0b010 != 0,
			i & 0b001 != 0,
		);

		let pos_offset = Vec3A::select(mask, velocity.signum(), Vec3A::ZERO);
		let pos_check = (pos + pos_offset).as_i64vec3();

		if let Some(v) = chunks.get_voxel(pos_check) {
			// zero the velocity in the affected axes
			velocity = Vec3A::select(mask, Vec3A::ZERO, velocity);

			// TODO: handle collision?
		}
	}

	// println!("{velocity}, {} -> {pos}", obj.get_pos());

	let ret = PhysicsEvent {
		pos: pos + velocity,
		velocity,
		collision: None,
	};

	ret
}
