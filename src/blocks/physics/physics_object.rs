use glam::*;

pub trait PhysicsObject {
	fn get_pos(&self) -> Vec3A;
	fn get_velocity(&self) -> Vec3A;
}
