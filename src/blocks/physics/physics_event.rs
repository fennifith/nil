use glam::*;

use crate::data::*;

pub struct PhysicsEvent {
	pub pos: Vec3A,
	pub velocity: Vec3A,
	pub collision: Option<PhysicsCollision>,
}

pub enum PhysicsCollision {
	Voxel {
		velocity: Vec3A,
		voxel: Voxel,
	},
}
