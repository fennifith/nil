use std::collections::HashMap;
use crossbeam_channel::*;

use super::*;
use crate::events::world::*;

pub async fn tick_players(world: &mut World, out_chan: &Sender<WorldOut>) -> anyhow::Result<()> {
	// find players that need updating
	let players = world.players.iter()
		.map(|(id, p)| (p.chunk(), *id))
		.collect::<HashMap<_, _>>();

	for (chunk, id) in players {
		let chunk_group = world.get_chunk_group(chunk);
		let mut player = world.players.get_mut(&id).unwrap();

		// if the chunks aren't loaded yet, skip
		if chunk_group.chunks.len() < 27 {
			continue;
		}

		// update player velocity based on input
		player.tick_movement();

		// update physics state for player
		let physics = physics::tick_physics(player, &chunk_group);
		player.pos = physics.pos;
		player.velocity = physics.velocity;

		// send processed updates to the client/ui
		out_chan.send(WorldOut::UpdatePlayer {
			player: player.clone(),
		})?;
	}

	Ok(())
}

