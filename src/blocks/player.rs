use glam::*;
use uuid::Uuid;

use crate::ui::InputState;

use super::chunk::*;
// player speed; can only move once per 8 ticks?
// const MOVEMENT_COOLDOWN: u8 = 2;

const MOVEMENT_SPEED: f32 = 5.0;
const MOVEMENT_JUMP: f32 = 3.0;

#[derive(Clone, Debug)]
pub struct Player {
	pub id: Uuid,
	pub pos: Vec3A,
	pub velocity: Vec3A,
	pub input: InputState,
}

impl Default for Player {
	fn default() -> Self {
		Player {
			id: Uuid::parse_str("61eccee2-7035-4a9a-af53-861f75ec4f64").unwrap(),
			pos: Vec3A::new(0., 50., 0.),
			velocity: Vec3A::splat(0.0),
			input: InputState::default(),
		}
	}
}

impl Player {
	pub fn chunk(&self) -> IVec3 {
		pos_to_chunk(self.pos.as_i64vec3())
	}

	pub fn tick_movement(&mut self) {
		let velocity = Vec3A::new(self.input.movement_direction.x, 0., self.input.movement_direction.y) * MOVEMENT_SPEED;

		// use the max of either the current or updated velocity
		self.velocity = Vec3A::new(
			if velocity.x.abs() > self.velocity.x.abs() { velocity.x } else { self.velocity.x },
			if velocity.y.abs() > self.velocity.y.abs() { velocity.y } else { self.velocity.y },
			if velocity.z.abs() > self.velocity.z.abs() { velocity.z } else { self.velocity.z },
		);

		// TODO: should only work if player is standing on ground
		if self.input.is_jumping {
			self.velocity.y = self.velocity.y.max(MOVEMENT_JUMP);
		}
	}
}

impl super::physics::PhysicsObject for Player {
	fn get_velocity(&self) -> Vec3A {
		self.velocity.clone()
	}

	fn get_pos(&self) -> Vec3A {
		self.pos.clone()
	}
}
