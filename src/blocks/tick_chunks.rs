use tokio::sync::mpsc::Sender;

use super::*;
use crate::events::world::*;

pub fn tick_chunks(_world: &mut World, _out_chan: &mut Sender<WorldOut>) {
	// find chunks that need updating

	// send processed updates to the client/ui
}

