use glam::*;

use crate::data::*;

pub trait VoxelCollection {
	fn get_voxel<'a>(&'a self, pos: I64Vec3) -> Option<&'a Voxel>;
	fn get_voxel_by_id<'a>(&'a self, uuid: &uuid::Uuid) -> Option<&'a Voxel>;
}

pub trait VoxelCollectionMut {
	fn set_voxel(&mut self, voxel: Voxel) -> Result<(), String>;
	fn remove_voxel(&mut self, pos: I64Vec3) -> Option<Voxel>;
	fn remove_voxel_by_id(&mut self, uuid: uuid::Uuid) -> Option<Voxel>;
}
