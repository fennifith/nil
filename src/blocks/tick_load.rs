use std::collections::HashSet;
use glam::*;
use crossbeam_channel::*;
use rayon::prelude::*;

use super::*;
use crate::events::world::*;
use super::worldgen::WorldGenerator;

pub async fn tick_load(world: &mut World, world_chan: &Sender<WorldIn>, world_out: &Sender<WorldOut>) -> anyhow::Result<()> {
	// unload previous chunks by player distance
	let mut chunks_to_unload: Vec<IVec3> = vec![];
	let mut chunks_to_load: Vec<IVec3> = vec![];

	{
		let loaded_chunks = world.chunks.keys().cloned().collect::<HashSet<_>>();
		let player_chunks: Vec<IVec3> = world.players.iter().map(|(_, p)| p.chunk()).collect();

		'unload: for loaded_chunk_pos in &loaded_chunks {
			// if player is in (cubic) range, keep chunk loaded
			for player_chunk in &player_chunks {
				if (*player_chunk - *loaded_chunk_pos).abs().max_element() <= RENDER_DISTANCE {
					continue 'unload;
				}
			}

			// otherwise, unload & remove
			chunks_to_unload.push(*loaded_chunk_pos);
		}

		// load new chunks in radius of player
		for player_chunk in &player_chunks {
			for x in -RENDER_DISTANCE..=RENDER_DISTANCE {
				let dx = ((RENDER_DISTANCE.pow(2) - x.pow(2)).abs() as f32).sqrt() as i32;
				for y in -dx..dx {
					for z in -dx..=dx {
						let pos = *player_chunk + IVec3::new(x, y, z);
						if !loaded_chunks.contains(&pos) {
							chunks_to_load.push(pos);
						}
					}
				}
			}
		}
	}

	// remove chunks from the local map
	for chunk_pos in chunks_to_unload.iter() {
		world.chunks.remove(chunk_pos);

		// send removed chunks to the graphics side
		world_out.send(WorldOut::UnloadChunk {
			chunk_pos: *chunk_pos,
		})?;
	}

	// load+save new chunks (in parallel)
	// let chunks_loading = chunks_to_load.iter()
	// 	.map(|chunk_pos| {
	// 		scheduler_chunk_load(world, generator, game, *chunk_pos)
	// 	})
	// 	.collect::<Vec<_>>();
	// futures::future::try_join_all(chunks_loading).await?;

	let is_finished = match &world.task_load_chunks {
		Some(handle) => handle.is_finished(),
		_ => true,
	};

	// spawn a thread to load new chunks
	if is_finished && chunks_to_load.len() > 0 {
		println!("spawning new blocking task for {} chunks", chunks_to_load.len());
		let world_chan_1 = world_chan.clone();
		let handle = tokio::task::spawn_blocking(move || {
			tick_load_chunks(chunks_to_load, world_chan_1);
		});

		world.task_load_chunks = Some(handle);
	}

	Ok(())
}

pub fn tick_load_chunks(chunks_to_load: Vec<IVec3>, world_chan: Sender<WorldIn>) {
	let generator = WorldGenerator::new();

	chunks_to_load.par_iter()
		.for_each(|chunk_pos| {
			let chunk = generator.evaluate(*chunk_pos);

			world_chan.send(WorldIn::LoadChunk { chunk })
				.map_err(|e| eprintln!("tick_load_chunks: {e}"))
				.ok();
		});

	println!("finished");
}

