use std::ops::RangeBounds;
use std::time::Instant;
use std::collections::VecDeque;
use std::sync::Mutex;

use crate::data::voxel::Voxel;
use crate::util::CubeSide;
use glam::*;

#[derive(Clone, Debug)]
pub enum CreateConflictBehavior {
	Nothing,
	Replace,
	Nearest,
}

#[derive(Clone, Debug)]
pub enum WorldEventTarget {
	Pos(I64Vec3),
	UUID(uuid::Uuid),
}

#[derive(Clone, Debug)]
pub enum WorldEvent {
	Create {
		uuid: uuid::Uuid,
		pos: I64Vec3,
		value: i64,
		on_conflict: CreateConflictBehavior,
	},
	Move {
		target: WorldEventTarget,
		direction: CubeSide,
	},
	Change {
		target: WorldEventTarget,
		to: Voxel,
	},
	Destroy {
		target: WorldEventTarget,
	},
	Scheduled {
		time: Instant,
		events: Vec<WorldEvent>,
	},
	InvalidateVoxel {
		pos: I64Vec3,
	},
	InvalidateChunk {
		chunk: IVec3,
	},
}

pub struct WorldEvents {
	pub events: Mutex<VecDeque<WorldEvent>>,
}

impl WorldEvents {
	pub fn new() -> Self {
		Self {
			events: Mutex::new(VecDeque::new()),
		}
	}

	pub fn drain<R: RangeBounds<usize>>(&self, range: R) -> Result<Vec<WorldEvent>, String> {
		let mut events = self.events.lock().map_err(|e| format!("Unable to lock events mutex: {e}"))?;
		Ok(events.drain(range).collect())
	}

	pub fn push(&self, event: WorldEvent) -> Result<(), String> {
		let mut events = self.events.lock().map_err(|e| format!("Unable to lock events mutex: {e}"))?;
		events.push_back(event);
		Ok(())
	}

	pub fn push_front(&self, event: WorldEvent) -> Result<(), String> {
		let mut events = self.events.lock().map_err(|e| format!("Unable to lock events mutex: {e}"))?;
		events.push_front(event);
		Ok(())
	}

	pub fn pop(&self) -> Option<WorldEvent> {
		self.events.lock()
			.map(|mut e| e.pop_front())
			.ok()
			.flatten()
	}
}
