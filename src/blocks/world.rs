use std::collections::HashMap;

use glam::*;
use tokio::task::JoinHandle;

use super::chunk_group::*;
use super::chunk::*;
use super::player::Player;

pub const RENDER_DISTANCE: i32 = 8;

pub struct World {
	pub players: HashMap<uuid::Uuid, Player>,
	pub chunks: HashMap<IVec3, LoadedChunk>,
	pub task_load_chunks: Option<JoinHandle<()>>,
}

impl World {
	pub fn new() -> Self {
		Self {
			players: HashMap::new(),
			chunks: HashMap::new(),
			task_load_chunks: None,
		}
	}
}

impl World {
	pub fn player_spawn(&mut self, player: Player) -> Result<(), String> {
		// TODO: locate a space to spawn the player
		self.players.insert(player.id, player);

		Ok(())
	}

	pub fn insert_chunk(&mut self, chunk: LoadedChunk) {
		self.chunks.insert(chunk.chunk.pos(), chunk);
	}

	pub fn get_chunk_group(&self, pos: IVec3) -> ChunkGroup<LoadedChunk> {
		let mut chunks: Vec<LoadedChunk> = Vec::new();
		for x in (pos.x-1)..(pos.x+2) {
			for y in (pos.y-1)..(pos.y+2) {
				for z in (pos.z-1)..(pos.z+2) {
					let chunk = match self.chunks.get(&IVec3::new(x, y, z)) {
						Some(c) => c,
						_ => continue,
					};

					chunks.push(chunk.clone());
				}
			}
		}

		let mut group = ChunkGroup::default();
		for chunk in chunks {
			group.chunks.insert(chunk.chunk.pos(), chunk);
		}

		group
	}
}
