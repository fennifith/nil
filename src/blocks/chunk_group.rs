use std::collections::HashMap;
use glam::*;

use super::voxel_collection::*;
use super::chunk::*;
use crate::data::*;

/**
 * Intended as a short-term structure for "a group of chunks" that can be
 * held as references all at once, during a query or operation.
 *
 * This prevents race conditions when working with rules; e.g. checks
 * involving multiple chunks can lock all chunks in this structure.
 */
pub struct ChunkGroup<C> {
	pub chunks: HashMap<IVec3, C>,
}

impl <C> std::default::Default for ChunkGroup<C> {
	fn default() -> Self {
		Self {
			chunks: HashMap::new(),
		}
	}
}

impl <C> ChunkGroup<C> {
	pub fn get(&self, pos: IVec3) -> Option<&C> {
		self.chunks.get(&pos)
	}
}

impl VoxelCollection for ChunkGroup<LoadedChunk> {
	fn get_voxel<'a>(&'a self, pos: I64Vec3) -> Option<&'a Voxel> {
		let pos_chunk = pos_to_chunk(pos);

		match self.chunks.get(&pos_chunk) {
			Some(c) => c.get_voxel(pos),
			_ => None,
		}
	}

	fn get_voxel_by_id<'a>(&'a self, uuid: &uuid::Uuid) -> Option<&'a Voxel> {
		for (_, chunk) in self.chunks.iter() {
			let result = chunk.get_voxel_by_id(uuid);

			if result.is_some() {
				return result;
			}
		}

		None
	}
}

impl VoxelCollectionMut for ChunkGroup<LoadedChunk> {
	fn set_voxel(&mut self, voxel: Voxel) -> Result<(), String> {
		let pos_chunk = pos_to_chunk(voxel.pos());
		let chunk = match self.chunks.get_mut(&pos_chunk) {
			Some(c) => c,
			_ => return Err(format!("Chunk group does not contain chunk {pos_chunk}")),
		};

		chunk.set_voxel(voxel)
	}

	fn remove_voxel(&mut self, pos: I64Vec3) -> Option<Voxel> {
		let pos_chunk = pos_to_chunk(pos);
		let chunk = match self.chunks.get_mut(&pos_chunk) {
			Some(c) => c,
			_ => return None,
		};

		chunk.remove_voxel(pos)
	}

	fn remove_voxel_by_id(&mut self, uuid: uuid::Uuid) -> Option<Voxel> {
		for (_, chunk) in self.chunks.iter_mut() {
			let result = chunk.remove_voxel_by_id(uuid);

			if result.is_some() {
				return result;
			}
		}

		None
	}
}

// pictured: me, appealing to the Rust type system - why on earth isn't this handled by a built-in trait?
impl AsRef<LoadedChunk> for &LoadedChunk {
	fn as_ref(&self) -> &LoadedChunk {
		self
    }
}
impl AsRef<LoadedChunk> for &mut LoadedChunk {
	fn as_ref(&self) -> &LoadedChunk {
		self
    }
}
impl AsMut<LoadedChunk> for &mut LoadedChunk {
	fn as_mut(&mut self) -> &mut LoadedChunk {
		self
    }
}

