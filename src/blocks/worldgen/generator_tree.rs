use std::{collections::HashMap, borrow::Borrow};
use glam::*;

use crate::CHUNK_SIZE;
use crate::data::Voxel;
use crate::blocks::{chunk::*, VoxelCollectionMut};

use super::*;

pub struct Generator {
	noise_fns: HashMap<String, Box<dyn noise::NoiseFn<f64, 3>>>,
	branches: GeneratorTree,
}

impl Generator {
	pub fn new(config_yaml: String) -> Result<Self, String> {
		let config: GeneratorConfig = serde_yaml::from_str(&config_yaml)
			.map_err(|e| format!("{e}"))?;

		let noise_fns = config.noise.iter()
			.map(|(key, noise)| (key.clone(), noise.assemble(1)))
			.collect();

		Ok(Self {
			noise_fns,
			branches: config.output,
		})
	}

	pub fn evaluate_tree(&self, pos: &I64Vec3, noise_values: &HashMap<String, f64>, tree: &GeneratorTree) -> Option<Voxel> {
		match tree {
			GeneratorTree::Sequence(v) => {
				v.iter()
					.filter_map(|branch| self.evaluate_tree(pos, noise_values, branch))
					.next()
			},
			GeneratorTree::IfTrue { if_true, then } => {
				if if_true.evaluate(pos, noise_values) {
					self.evaluate_tree(pos, noise_values, then.borrow())
				} else {
					None
				}
			},
			GeneratorTree::IfFalse { if_false, then } => {
				if !if_false.evaluate(pos, noise_values) {
					self.evaluate_tree(pos, noise_values, then.borrow())
				} else {
					None
				}
			},
			GeneratorTree::Value { material } => {
				if *material == 'a' {
					None
				} else {
					Some(Voxel::new(pos.clone(), *material as u8))
				}
			}
		}
	}

	pub fn evaluate_pos(&self, pos: I64Vec3) -> Option<Voxel> {
		let mut noise_values: HashMap<String, f64> = self.noise_fns.iter()
			.map(|(key, noise)| (key.clone(), noise.get([pos.x as f64, pos.y as f64, pos.z as f64])))
			.collect();

		noise_values.insert("x".to_string(), pos.x as f64);
		noise_values.insert("y".to_string(), pos.y as f64);
		noise_values.insert("z".to_string(), pos.z as f64);

		self.evaluate_tree(&pos, &noise_values, &self.branches)
	}

	pub fn evaluate(&self, pos_chunk: IVec3) -> LoadedChunk {
		let mut chunk = LoadedChunk::new(pos_chunk);

		for x in 0..CHUNK_SIZE {
			for y in 0..CHUNK_SIZE {
				for z in 0..CHUNK_SIZE {
					let pos = pos_to_absolute(UVec3::new(x, y, z), pos_chunk);
					let voxel = self.evaluate_pos(pos);

					if let Some(v) = voxel {
						chunk.set_voxel(v).unwrap();
					}
				}
			}
		}

		chunk
	}
}
