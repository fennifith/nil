use std::cell::RefCell;
use glam::*;
use super::Generator;

use crate::blocks::LoadedChunk;

pub struct WorldGenerator {
}

thread_local! {
	static GENERATOR: RefCell<Generator> = RefCell::new(Generator::new(include_str!("generator.yaml").to_string()).unwrap());
}

impl WorldGenerator {
	pub fn new() -> Self {
		Self { }
	}

	pub fn evaluate(&self, pos_chunk: IVec3) -> LoadedChunk {
		GENERATOR.with(|g| {
			let generator = g.borrow();

			generator.evaluate(pos_chunk)
		})
	}
}
