use glam::*;
use std::collections::HashMap;


#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum Value {
	Named(String),
	Constant(f64),
}

impl Value {
	pub fn evaluate(&self, noise_values: &HashMap<String, f64>) -> f64 {
		match self {
			Self::Named(s) => *noise_values.get(s).unwrap(),
			Self::Constant(f) => *f,
		}
	}
}

#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum Condition {
	GreaterThan {
		it: Value,
		greater_than: Value,
	},
	LessThan {
		it: Value,
		less_than: Value,
	},
	AnyOf {
		any_of: Vec<Condition>,
	},
	AllOf {
		all_of: Vec<Condition>,
	},
}

impl Condition {
	pub fn evaluate(&self, pos: &I64Vec3, noise_values: &HashMap<String, f64>) -> bool {
		match self {
			Condition::AllOf { all_of } => {
				all_of.into_iter()
					.all(|c| c.evaluate(pos, noise_values))
			},
			Condition::AnyOf { any_of } => {
				any_of.into_iter()
					.any(|c| c.evaluate(pos, noise_values))
			},
			Condition::GreaterThan { it, greater_than } => {
				it.evaluate(noise_values) > greater_than.evaluate(noise_values)
			},
			Condition::LessThan { it, less_than } => {
				it.evaluate(noise_values) < less_than.evaluate(noise_values)
			},
		}
	}
}


#[derive(serde::Deserialize)]
#[serde(untagged)]
pub enum GeneratorTree {
	Sequence(Vec<GeneratorTree>),
	IfTrue {
		if_true: Condition,
		then: Box<GeneratorTree>,
	},
	IfFalse {
		if_false: Condition,
		then: Box<GeneratorTree>,
	},
	Value {
		material: char,
	}
}

#[derive(serde::Deserialize)]
pub enum NoiseFunction {
	Abs(Box<NoiseFunction>),
	Add(Box<NoiseFunction>, Box<NoiseFunction>),
	Constant(f64),
	Multiply(Box<NoiseFunction>, Box<NoiseFunction>),
	Blend {
		a: Box<NoiseFunction>,
		b: Box<NoiseFunction>,
		control: Box<NoiseFunction>,
	},
	ScalePoint {
		value: Box<NoiseFunction>,
		x: f64,
		y: f64,
		z: f64,
	},
	Billow {
		octaves: usize,
		frequency: f64,
		lacunarity: f64,
		persistence: f64,
	},
	Worley,
}

impl NoiseFunction {
	pub fn assemble(&self, seed: u32) -> Box<dyn noise::NoiseFn<f64, 3>> {
		match self {
			NoiseFunction::Abs(f) => {
				Box::new(noise::Abs::new(f.assemble(seed)))
			},
			NoiseFunction::Add(a, b) => {
				Box::new(noise::Add::new(
					a.assemble(seed),
					b.assemble(seed),
				))
			},
			NoiseFunction::Blend { a, b, control } => {
				Box::new(noise::Blend::new(
					a.assemble(seed),
					b.assemble(seed),
					control.assemble(seed),
				))
			},
			NoiseFunction::Multiply(a, b) => {
				Box::new(noise::Multiply::new(
					a.assemble(seed),
					b.assemble(seed),
				))
			},
			NoiseFunction::Constant(c) => {
				Box::new(noise::Constant::new(*c))
			},
			NoiseFunction::ScalePoint { value, x, y, z } => {
				Box::new(
					noise::ScalePoint::new(value.assemble(seed))
					.set_all_scales(*x, *y, *z, 1.)
				)
			},
			NoiseFunction::Billow { octaves, frequency, lacunarity, persistence } => {
				let mut noise = noise::Billow::<noise::Perlin>::new(seed);
				noise.octaves = *octaves;
				noise.frequency = *frequency;
				noise.lacunarity = *lacunarity;
				noise.persistence = *persistence;
				Box::new(noise)
			},
			NoiseFunction::Worley => {
				Box::new(noise::Worley::new(seed))
			},
		}
	}
}

#[derive(serde::Deserialize)]
pub struct GeneratorConfig {
	pub noise: HashMap<String, NoiseFunction>,
	pub output: GeneratorTree,
}
