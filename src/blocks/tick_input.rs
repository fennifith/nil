use crossbeam_channel::{Sender, Receiver};

use super::*;
use crate::events::world::*;

fn poll_channel<T>(receiver: &mut Receiver<T>) -> Vec<T> {
	let mut ret = vec![];

	while let Ok(e) = receiver.try_recv() {
		ret.push(e);
	}

	ret
}

pub async fn tick_input(world: &mut World, in_chan: &mut Receiver<WorldIn>, out_chan: &Sender<WorldOut>) -> anyhow::Result<()> {
	let events = poll_channel(in_chan);

	// append input events to the chunk-specific event queue(s), and mark invalid
	for event in events {
		match event {
			WorldIn::LoadChunk { chunk } => {
				world.insert_chunk(chunk.clone());
				out_chan.send(WorldOut::UpdateChunk { chunk })?;
			},
			WorldIn::PlayerInput { uuid, input } => {
				let mut player = match world.players.get_mut(&uuid) {
					Some(p) => p,
					_ => continue,
				};

				player.input = input;
			},
		}
	}

	// handle player movement
	// - should be pushed to per-chunk event queues...
	Ok(())
}

