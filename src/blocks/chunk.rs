use glam::*;
use std::collections::{HashMap, HashSet};

use crate::constants::CHUNK_SIZE;
use crate::data::*;
use crate::util::octree::Octree;

use super::voxel_collection::*;

pub fn pos_to_chunk(pos: I64Vec3) -> IVec3 {
	let chunk_size = CHUNK_SIZE as i64;
	IVec3::new(
		((pos.x + if pos.x < 0 { -chunk_size+1_i64 } else { 0 }) / chunk_size) as i32,
		((pos.y + if pos.y < 0 { -chunk_size+1_i64 } else { 0 }) / chunk_size) as i32,
		((pos.z + if pos.z < 0 { -chunk_size+1_i64 } else { 0 }) / chunk_size) as i32,
	)
}

pub fn pos_in_chunk(pos: I64Vec3) -> UVec3 {
	(pos - (pos_to_chunk(pos).as_i64vec3() * (CHUNK_SIZE as i64))).as_uvec3()
}

pub fn pos_to_absolute(pos: UVec3, chunk: IVec3) -> I64Vec3 {
	pos_from_chunk(chunk) + pos.as_i64vec3()
}

pub fn pos_to_index(pos: UVec3) -> Result<usize, String> {
	let chunk_size = CHUNK_SIZE;

	if pos.x >= chunk_size || pos.y >= chunk_size || pos.z >= chunk_size {
		return Err(format!("vec {pos} is larger than CHUNK_SIZE {CHUNK_SIZE}"));
	}

	Ok(
		pos.x as usize
		+ (pos.y * chunk_size) as usize
		+ (pos.z * chunk_size.pow(2)) as usize
	)
}

pub fn pos_from_index(index: usize) -> UVec3 {
	let i = index as u32;
	let chunk_size = CHUNK_SIZE;

	UVec3::new(
		i % chunk_size,
		(i / chunk_size) % chunk_size,
		(i / chunk_size.pow(2)) % chunk_size,
	)
}

pub fn pos_from_chunk(chunk: IVec3) -> I64Vec3 {
	chunk.as_i64vec3() * (CHUNK_SIZE as i64)
}

#[derive(Clone, Debug)]
pub struct LoadedChunk {
	pub chunk: Chunk,
	pub voxel_map: HashMap<uuid::Uuid, Voxel>,
	pub voxels: Octree<uuid::Uuid>,
	pub updates: HashSet<UVec3>,
}

impl std::default::Default for LoadedChunk {
	fn default() -> Self {
		Self {
			chunk: Chunk::new(IVec3::new(0, 0, 0)),
			voxel_map: HashMap::new(),
			voxels: Octree::with_length(CHUNK_SIZE as usize),
			updates: HashSet::new(),
		}
	}
}

impl LoadedChunk {
	pub fn new(pos: IVec3) -> LoadedChunk {
		LoadedChunk {
			chunk: Chunk::new(pos),
			..LoadedChunk::default()
		}
	}

	fn pos_local(&self, pos: I64Vec3) -> Result<UVec3, String> {
		let pos_chunk = pos_to_chunk(pos);
		if pos_chunk != self.chunk.pos() {
			return Err(format!("Voxel is not inside the current chunk"));
		}

		Ok(pos_in_chunk(pos))
	}
}

impl VoxelCollection for LoadedChunk {
	fn get_voxel<'a>(&'a self, pos: I64Vec3) -> Option<&'a Voxel> {
		let voxel_pos = self.pos_local(pos).unwrap();

		let uuid = match self.voxels.get(voxel_pos) {
			Some(u) => u,
			_ => return None,
		};

		self.get_voxel_by_id(uuid)
	}

	fn get_voxel_by_id<'a>(&'a self, uuid: &uuid::Uuid) -> Option<&'a Voxel> {
		self.voxel_map.get(uuid)
	}
}

impl VoxelCollectionMut for LoadedChunk {
	fn set_voxel(&mut self, voxel: Voxel) -> Result<(), String> {
		let voxel_pos = self.pos_local(voxel.pos()).unwrap();

		self.voxels.insert(voxel_pos, voxel.uuid);
		self.voxel_map.insert(voxel.uuid, voxel);
		Ok(())
	}

	fn remove_voxel(&mut self, pos: I64Vec3) -> Option<Voxel> {
		let voxel_pos = self.pos_local(pos).unwrap();

		let uuid = match self.voxels.remove(voxel_pos) {
			Some(u) => u,
			_ => return None,
		};

		self.remove_voxel_by_id(uuid)
	}

	fn remove_voxel_by_id(&mut self, uuid: uuid::Uuid) -> Option<Voxel> {
		let voxel = self.voxel_map.remove(&uuid);

		if let Some(v) = &voxel {
			self.voxels.remove(self.pos_local(v.pos()).unwrap());
		}

		voxel
	}
}
