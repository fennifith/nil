#![allow(dead_code)]

hypermod::hypermod!();

fn main() -> Result<(), String> {
	game::run()
}
