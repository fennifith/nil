use std::sync::Arc;
use crossbeam_channel::{unbounded, Sender, Receiver};

use crate::blocks::*;
use crate::events::*;

pub struct GameState {
	pub player_id: uuid::Uuid,
}

impl std::default::Default for GameState {
	fn default() -> Self {
		Self {
			player_id: uuid::Uuid::new_v4(),
		}
	}
}

pub struct Game {
	pub state: Arc<GameState>,
}

impl Game {

	pub fn new() -> Result<Game, String> {
		let game = Game {
			state: Arc::new(GameState {
				// TODO: determine this id per-player/device
				player_id: Player::default().id,
				..GameState::default()
			}),
		};
		Ok(game)
	}

	pub async fn scheduler(&mut self, mut world_in_chan: (Sender<WorldIn>, Receiver<WorldIn>), world_out_chan: Sender<WorldOut>) {
		// TODO: remove db from world
		let mut world = World::new();
		// let db = PostgresDb::new("postgresql://nil:fixme@127.0.0.1:5432/nil").await.unwrap();

		// TODO: find somewhere else to spawn the player...
		let player: Player = Player::default();
		world.player_spawn(player).unwrap();

		let mut interval = tokio::time::interval(tokio::time::Duration::from_millis(100));
		interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Delay);

		loop {
			crate::blocks::tick_input(&mut world, &mut world_in_chan.1, &world_out_chan).await.unwrap();
			crate::blocks::tick_load(&mut world, &world_in_chan.0, &world_out_chan).await.unwrap();
			crate::blocks::tick_players(&mut world, &world_out_chan).await.unwrap();

			interval.tick().await;
		}
	}
}

pub fn run() -> Result<(), String> {
	let world_in_chan = unbounded::<WorldIn>();
	let world_in_chan_0 = world_in_chan.0.clone();

	let world_out_chan = unbounded::<WorldOut>();

	let mut game = Game::new()?;
	let game_state = game.state.clone();

	let threaded_rt = tokio::runtime::Runtime::new().unwrap();
	threaded_rt.spawn(async move {
		game.scheduler(world_in_chan, world_out_chan.0).await
	});

	crate::ui::graphics::state::run(game_state, world_in_chan_0, world_out_chan.1);
	Ok(())
}
