use std::f32::consts::PI;

use glam::*;

#[derive(Clone)]
pub struct Camera {
	pub pos: Vec3A,
	pub rot: Vec3A,
	pub rot_scale: f32,
	pub fovy: f32,
	pub aspect: f32,
	pub near: f32,
	pub far: f32,
	pub distance: f32,
	pub distance_follow: f32,
}

impl Camera {
	pub fn new() -> Camera {
		Camera {
			pos: Vec3A::new(0.0, 0.0, 0.0),
			rot: Vec3A::new(0.0, 0.0, 0.0),
			rot_scale: 0.005,
			fovy: PI / 2.0,
			aspect: 1.0,
			near: 0.1,
			far: 200.0,
			distance: 48.0,
			distance_follow: 8.0,
		}
	}

	pub fn resize(&mut self, width: u32, height: u32) {
		self.aspect = width as f32 / height as f32;
	}

	pub fn move_to(&mut self, pos: Vec3A, rot: Vec3A, speed: f32) {
		self.pos = self.pos*(1.0-speed) + pos*speed;
		self.rot = self.rot*(1.0-speed) + rot*speed;
	}

	pub fn follow(&mut self, pos: Vec3A, rot: Vec3A, speed: f32) {
		let pos_next = Vec3A::new(
			self.pos.x.clamp(pos.x - self.distance_follow, pos.x + self.distance_follow),
			pos.y,
			self.pos.z.clamp(pos.z - self.distance_follow, pos.z + self.distance_follow),
		);

		self.move_to(pos_next, rot, speed);
	}

	pub fn update_mouse(&mut self, dx: f64, dy: f64) {
		let max_x = PI;
		let max_y = PI*0.5;

		self.rot.x += (-dx * self.rot_scale as f64) as f32;
		if self.rot.x > max_x { self.rot.x -= max_x*2.0; }
		if self.rot.x < -max_x { self.rot.x += max_x*2.0; }

		self.rot.y = (self.rot.y + (-dy * self.rot_scale as f64) as f32).clamp(-max_y, max_y);
	}

	fn translation_mat4(&self) -> Mat4 {
		// move scene away from camera, for "third person"
		let rotation = self.rotation_mat4().inverse();
		let direction = rotation.mul_vec4(Vec4::new(0.0, 0.0, -self.distance, 1.0)).truncate();

		Mat4::from_translation(Vec3::from(-self.pos) + direction)
	}

	fn rotation_mat4(&self) -> Mat4 {
		(
			Mat4::from_rotation_y(self.rot.x) *
			Mat4::from_rotation_x(self.rot.y)
		).inverse()
	}

	fn projection_mat4(&self) -> Mat4 {
		Mat4::perspective_rh(self.fovy, self.aspect, self.near, self.far)
	}

	pub fn to_view_mat4(&self) -> [f32; 16] {
		let view = self.rotation_mat4() * self.translation_mat4();

		// extract matrix data into slice
		let mut slice: [f32; 16] = [0.0; 16];
		for (i, f) in view.to_cols_array().iter().enumerate() {
			slice[i] = *f;
		}

		slice
	}

	pub fn to_proj_mat4(&self) -> [f32; 16] {
		let proj = self.projection_mat4();

		// extract matrix data into slice
		let mut slice: [f32; 16] = [0.0; 16];
		for (i, f) in proj.to_cols_array().iter().enumerate() {
			slice[i] = *f;
		}

		slice
	}

	pub fn to_uniform(&self) -> CameraUniform {
		CameraUniform {
			view: self.to_view_mat4(),
			proj: self.to_proj_mat4(),
		}
	}
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct CameraUniform {
	view: [f32; 16],
	proj: [f32; 16],
}
