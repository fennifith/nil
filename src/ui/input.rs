use std::collections::{HashSet, HashMap};

use winit::event::*;
use glam::*;

use crate::ui::camera::Camera;

enum KeyAction {
	Move(Vec2),
	Jump,
	Crouch,
}

lazy_static::lazy_static! {
	static ref KEY_BINDS: HashMap<VirtualKeyCode, KeyAction> = HashMap::from_iter([
		(VirtualKeyCode::W, KeyAction::Move(Vec2::new(0., -1.))),
		(VirtualKeyCode::A, KeyAction::Move(Vec2::new(-1., 0.))),
		(VirtualKeyCode::S, KeyAction::Move(Vec2::new(0., 1.))),
		(VirtualKeyCode::D, KeyAction::Move(Vec2::new(1., 0.))),
		(VirtualKeyCode::Space, KeyAction::Jump),
		(VirtualKeyCode::LShift, KeyAction::Crouch),
	].into_iter());
}

#[derive(Clone, Debug)]
pub struct InputState {
	pub movement_direction: Vec2,
	pub is_jumping: bool,
	pub is_crouching: bool,
}

impl std::default::Default for InputState {
	fn default() -> Self {
		Self {
			movement_direction: Vec2::splat(0.),
			is_jumping: false,
			is_crouching: false,
		}
	}
}

pub struct Input {
	pub key_states: HashSet<VirtualKeyCode>,
	pub is_invalid: bool,
}

impl Input {
	pub fn new() -> Input {
		Input {
			key_states: HashSet::new(),
			is_invalid: false,
		}
	}

	pub fn get_movement_direction(&self, camera: &Camera) -> Vec2 {
		let movement_dir = Mat4::from_rotation_y(0.0);

		let mut vec = Vec2::new(0.0, 0.0);
		for key in &self.key_states {
			if let Some(KeyAction::Move(dir)) = KEY_BINDS.get(key) {
				vec += *dir;
			}
		}

		// rotate & normalize the movement direction
		let movement_raw = if vec.max_element() > 0.0 {
			movement_dir.transform_point3(Vec3::new(vec.x, 0.0, vec.y))
		} else {
			Vec3::new(vec.x, 0.0, vec.y)
		};

		// rotate movement by camera direction
		Mat4::from_rotation_y(camera.rot.x).transform_point3(movement_raw).xz()
	}

	pub fn get_state(&self, camera: &Camera) -> InputState {
		let mut state = InputState::default();

		for key in &self.key_states {
			let action = match KEY_BINDS.get(key) {
				Some(a) => a,
				_ => continue,
			};

			match action {
				KeyAction::Jump => {
					state.is_jumping = true;
				},
				KeyAction::Crouch => {
					state.is_crouching = true;
				},
				_ => (),
			}
		}

		state.movement_direction = self.get_movement_direction(camera);

		state
	}

	pub fn handle_event(&mut self, event: &DeviceEvent) {
		match event {
			DeviceEvent::Key(
				KeyboardInput {
					virtual_keycode: Some(key),
					state,
					..
				}
			) => {
				match state {
					ElementState::Pressed => {
						// on matching key press, push to event queue
						self.key_states.insert(key.clone());
						self.is_invalid = true;
					},
					ElementState::Released => {
						self.key_states.remove(key);
						self.is_invalid = true;
					},
				}
			},
			_ => {},
		}
	}
}
