use wgpu::{*, util::*};

pub fn create_uniform_buffer<T: Clone + bytemuck::Pod, S: AsRef<str>>(
	device: &Device,
	value: &T,
	visibility: ShaderStages,
	label: S
) -> (Buffer, BindGroupLayout, BindGroup) {
	let buffer = device.create_buffer_init(&util::BufferInitDescriptor {
		label: Some(label.as_ref()),
		contents: bytemuck::cast_slice(&[value.clone()]),
		usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
	});

	let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
		entries: &[BindGroupLayoutEntry {
			binding: 0,
			visibility,
			ty: BindingType::Buffer {
				ty: BufferBindingType::Uniform,
				has_dynamic_offset: false,
				min_binding_size: None,
			},
			count: None,
		}],
		label: Some(&format!("{}_bind_group_layout", label.as_ref())),
	});

	let bind_group = device.create_bind_group(&BindGroupDescriptor {
		layout: &bind_group_layout,
		entries: &[BindGroupEntry {
			binding: 0,
			resource: buffer.as_entire_binding(),
		}],
		label: Some(&format!("{}_bind_group", label.as_ref())),
	});

	(buffer, bind_group_layout, bind_group)
}
