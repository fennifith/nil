use wgpu::*;
use glam::*;

use super::buffer::*;

pub struct Vertex {
	pub position: Vec3,
	pub normal: Vec3,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct VertexBytes {
	position: [f32; 3],
	normal: [f32; 3],
}

impl ToBytes<VertexBytes> for Vertex {
	fn to_bytes(&self) -> VertexBytes {
		VertexBytes {
			position: self.position.to_array(),
			normal: self.normal.to_array(),
		}
	}
}

impl BufferLayout for Vertex {
	fn buffer_layout<'a>() -> VertexBufferLayout<'a> {
		use std::mem;
		VertexBufferLayout {
			array_stride: mem::size_of::<VertexBytes>() as BufferAddress,
			step_mode: VertexStepMode::Vertex,
			attributes: &[
				VertexAttribute {
					offset: 0,
					shader_location: 0,
					format: VertexFormat::Float32x3,
				},
				VertexAttribute {
					offset: mem::size_of::<[f32; 3]>() as BufferAddress,
					shader_location: 1,
					format: VertexFormat::Float32x3,
				},
			],
		}
	}
}

