use wgpu::*;
use glam::*;

use super::buffer::*;

pub struct Instance {
	pub pos: Vec3,
	pub color: Vec3,
}

impl Instance {
	pub fn new(pos: Vec3, material: u8) -> Self {
		let color = match material {
			b'p' => Vec3::new(0.3, 0.3, 1.),
			2 => Vec3::new(1.0, 0.3, 0.3),
			_ => Vec3::new(0.3, 0.3, 0.3),
		};

		Self {
			pos,
			color,
		}
	}
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct InstanceBytes {
	pos: [f32; 3],
	color: [f32; 3],
}

impl ToBytes<InstanceBytes> for Instance {
	fn to_bytes(&self) -> InstanceBytes {
		InstanceBytes {
			pos: self.pos.to_array(),
			color: self.color.to_array(),
		}
	}
}

impl BufferLayout for Instance {
	fn buffer_layout<'a>() -> VertexBufferLayout<'a> {
		use std::mem;
		VertexBufferLayout {
			array_stride: mem::size_of::<InstanceBytes>() as BufferAddress,
			step_mode: VertexStepMode::Instance,
			attributes: &[
				VertexAttribute {
					offset: 0,
					shader_location: 2,
					format: VertexFormat::Float32x3,
				},
				VertexAttribute {
					offset: mem::size_of::<[f32; 3]>() as BufferAddress,
					shader_location: 3,
					format: VertexFormat::Float32x3,
				},
			],
		}
	}
}
