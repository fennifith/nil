use std::marker::PhantomData;
use std::hash::Hash;

use glam::IVec3;
use wgpu::*;

use crate::util::allocator::Allocator;

use super::{buffer_vertex::*, Instance, InstanceBytes};

pub trait BufferLayout {
	fn buffer_layout<'a>() -> VertexBufferLayout<'a>;
}

pub trait ToBytes<T> where T: bytemuck::Pod {
	fn to_bytes(&self) -> T;
}

pub type VertexBuffer = ManagedBuffer<Vertex, VertexBytes>;
pub type InstanceBuffer = SegmentedBuffer<IVec3, Instance, InstanceBytes>;

pub struct ManagedBuffer<T, B> {
	pub label: String,
	pub buffer: Buffer,
	pub buffer_size: BufferAddress,
	pub size: usize,
	_t: PhantomData<T>,
	_b: PhantomData<B>,
}

impl <T, B> ManagedBuffer<T, B> where T: BufferLayout + ToBytes<B>, B: bytemuck::Pod {
	pub fn new<S: AsRef<str>>(label: S, device: &Device, size: BufferAddress) -> Self {
		let size_bytes = size * std::mem::size_of::<B>() as BufferAddress;

		// create a new instance buffer
		let align_mask = COPY_BUFFER_ALIGNMENT - 1;
		let buffer = device.create_buffer(&BufferDescriptor {
			label: Some(&format!("{}_segmented_buffer", label.as_ref())),
			size: ((size_bytes + align_mask) & !align_mask), // round up to nearest multiple of COPY_BUFFER_ALIGNMENT
			usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		Self {
			label: label.as_ref().to_string(),
			buffer,
			buffer_size: size_bytes,
			size: 0,
			_t: PhantomData,
			_b: PhantomData,
		}
	}

	pub fn write(&mut self, queue: &Queue, vec: &Vec<T>) -> Result<(), String> {
		// try to allocate the required size
		let mut instances: Vec<B> = vec.iter().map(|i| i.to_bytes()).collect();
		instances.shrink_to_fit();

		// TODO: actually resize the buffer if this happens, perhaps?
		if (instances.len() * std::mem::size_of::<B>()) as u64 > self.buffer_size {
			return Err(format!("Data extends beyond max buffer."));
		}

		// write instances to buffer segment
		queue.write_buffer(
			&self.buffer,
			0,
			bytemuck::cast_slice(&instances)
		);

		self.size = instances.len();

		Ok(())
	}
}

pub struct SegmentedBuffer<K, T, B> {
	pub label: String,
	pub allocator: Allocator<K>,
	pub buffer: Buffer,
	pub buffer_size: BufferAddress,
	_t: PhantomData<T>,
	_b: PhantomData<B>,
}

impl <K, T, B> SegmentedBuffer<K, T, B> where K: Clone + Eq + Hash + std::fmt::Debug, T: BufferLayout + ToBytes<B>, B: bytemuck::Pod {
	pub fn new<S: AsRef<str>>(label: S, device: &Device, size: BufferAddress) -> Self {
		let size_bytes = size * std::mem::size_of::<B>() as BufferAddress;

		// create a new instance buffer
		let align_mask = COPY_BUFFER_ALIGNMENT - 1;
		let buffer = device.create_buffer(&BufferDescriptor {
			label: Some(&format!("{}_segmented_buffer", label.as_ref())),
			size: ((size_bytes + align_mask) & !align_mask), // round up to nearest multiple of COPY_BUFFER_ALIGNMENT
			usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
			mapped_at_creation: false,
		});

		Self {
			label: label.as_ref().to_string(),
			allocator: Allocator::new(1),
			buffer,
			buffer_size: size_bytes,
			_t: PhantomData,
			_b: PhantomData,
		}
	}

	pub fn remove(&mut self, key: K) {
		self.allocator.deallocate(key).ok();
	}

	pub fn write(&mut self, queue: &Queue, key: K, vec: &Vec<T>) -> Result<(), String> {
		// try to allocate the required size
		let offset = self.allocator.allocate(key, vec.len());
		let mut instances: Vec<B> = vec.iter().map(|i| i.to_bytes()).collect();
		instances.shrink_to_fit();

		// TODO: actually resize the buffer if this happens, perhaps?
		if (self.allocator.size() * std::mem::size_of::<B>()) as u64 > self.buffer_size {
			return Err(format!("Segment {offset} extends beyond max buffer."));
		}

		// write instances to buffer segment
		queue.write_buffer(
			&self.buffer,
			offset as BufferAddress * std::mem::size_of::<B>() as BufferAddress,
			bytemuck::cast_slice(&instances)
		);

		Ok(())
	}
}
