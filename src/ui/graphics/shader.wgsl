// Vertex shader

struct Camera {
	view: mat4x4<f32>,
	proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: Camera;

struct Light {
	pos: vec4<f32>,
	color: vec4<f32>,
	attenuation: vec4<f32>,
};
struct Lighting {
	ambient: vec3<f32>,

	lights_num: u32,
	lights: array<Light, 32>,

	fog: vec3<f32>,
	fog_distance: f32,
};
@group(1) @binding(0)
var<uniform> lighting: Lighting;

struct VertexInput {
	@location(0) position: vec3<f32>,
	@location(1) normal: vec3<f32>,
};

// struct InstanceInput { uint id; vec3 pos; vec4 rot; vec4 tex; }
struct InstanceInput {
	@location(2) position: vec3<f32>,
	@location(3) color: vec3<f32>,
};

struct VertexOutput {
	@builtin(position) position: vec4<f32>,
	@location(0) color: vec3<f32>,
	@location(1) light: vec3<f32>,
	@location(2) distance: f32,
};

fn get_lighting(P: vec3<f32>, N: vec3<f32>) -> vec3<f32> {
	var light = vec3<f32>(0.0);

	for (var i = 0u; i < lighting.lights_num; i = i + 1u) {
		let L = lighting.lights[i].pos.xyz - P;
		let dist = length(L);

		let attenuation_vars = lighting.lights[i].attenuation;
		let attenuation = 1.0 / (attenuation_vars[0] + (attenuation_vars[1] * dist) + (attenuation_vars[2] * dist * dist));

		let diffuse = max(dot(N, normalize(L)), 0.0) * attenuation;

		light = light + (lighting.lights[i].color.rgb * diffuse);
	}

	return light;
}

fn vec3_rotate(v: vec3<f32>, q: vec4<f32>) -> vec3<f32> {
	let temp = cross(q.xyz, v) + q.w * v;
	let rotated = v + 2.0*cross(q.xyz, temp);
	return rotated;
}

@vertex
fn vs_main(
	vertex: VertexInput,
	instance: InstanceInput,
) -> VertexOutput {
	var out: VertexOutput;

	// projected position
	out.position = camera.proj * camera.view * vec4<f32>(instance.position + vertex.position, 1.0);

	out.color = instance.color;

	out.light = get_lighting(instance.position, normalize(vertex.normal));

	// vertex distance from player
	out.distance = length(camera.view * vec4<f32>(instance.position, 1.0));

	return out;
}

// Fragment shader

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
	let color = in.color;
	let alpha = clamp((1.0 - exp(1.5 - in.distance)) * 3.0, 0.0, 1.0);

	// return vec4<f32>(1.0, color.g, 0.5, 1.0);

	let out_color = vec4<f32>(
		color.rgb*(lighting.ambient + length(in.light)) + in.light,
		1.0,
	);

	// fog = 1 - e^(-2 * distance^2)
	let fog_distance = min(1.0, (lighting.fog_distance - in.distance) / lighting.fog_distance);
	let fog_ratio = clamp(1.0 - exp(-1.0 * exp2(fog_distance)), 0.0, 1.0);
	return vec4<f32>(
		(out_color.rgb * fog_ratio) + (lighting.fog * (1.0-fog_ratio)),
		out_color.a,
	);
}
