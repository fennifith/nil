#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Light {
	pub pos: [f32; 4],
	pub color: [f32; 4],
	pub attenuation: [f32; 4],
}

impl Default for Light {
	fn default() -> Self {
		Self {
			pos: [0.0; 4],
			color: [0.0; 4],
			// default attenuation
			attenuation: [1.0, 0.5, 0.2, 0.0],
		}
	}
}

pub const MAX_LIGHTS: usize = 32;

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Lighting {
	ambient: [f32; 3],

	lights_num: u32,
	lights: [Light; MAX_LIGHTS],

	fog: [f32; 3],
	fog_distance: f32,
}

impl Default for Lighting {
	fn default() -> Self {
		Self {
			ambient: [0.8; 3],
			lights_num: 0,
			lights: [Default::default(); MAX_LIGHTS],
			fog: [0.0, 0.0, 0.0],
			fog_distance: 30.0,
		}
	}
}

impl Lighting {
	pub fn update_lights(&mut self, lights: &Vec<Light>) {
		self.lights_num = lights.len().min(MAX_LIGHTS) as u32;

		for i in 0..MAX_LIGHTS {
			self.lights[i] = match lights.get(i) {
				Some(light) => light.clone(),
				None => Default::default(),
			}
		}
	}
}
