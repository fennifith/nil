use glam::*;

use crate::util::types::CubeSide;

use super::buffer_vertex::Vertex;

pub fn build_cube() -> Vec<Vertex> {
	let mut ret = Vec::new();
	for face in CubeSide::iter() {
		let quad: (Vec3, Vec3, Vec3, Vec3) = match face.to_string().as_str() {
			"left" => (
				Vec3::new(0., 0., 0.),
				Vec3::new(0., 1., 0.),
				Vec3::new(0., 0., 1.),
				Vec3::new(0., 1., 1.),
			),
			"top" => (
				Vec3::new(0., 1., 0.),
				Vec3::new(1., 1., 0.),
				Vec3::new(0., 1., 1.),
				Vec3::new(1., 1., 1.),
			),
			"front" => (
				Vec3::new(0., 0., 0.),
				Vec3::new(1., 0., 0.),
				Vec3::new(0., 1., 0.),
				Vec3::new(1., 1., 0.),
			),
			"right" => (
				Vec3::new(1., 0., 0.),
				Vec3::new(1., 0., 1.),
				Vec3::new(1., 1., 0.),
				Vec3::new(1., 1., 1.),
			),
			"bottom" => (
				Vec3::new(1., 0., 1.),
				Vec3::new(1., 0., 0.),
				Vec3::new(0., 0., 1.),
				Vec3::new(0., 0., 0.),
			),
			"back" => (
				Vec3::new(0., 0., 1.),
				Vec3::new(0., 1., 1.),
				Vec3::new(1., 0., 1.),
				Vec3::new(1., 1., 1.),
			),
			_ => panic!(),
		};

		let normal = face.to_vec();

		let vertices = [
			quad.3.clone(),
			quad.1.clone(),
			quad.0.clone(),
			quad.0.clone(),
			quad.2.clone(),
			quad.3.clone(),
		];

		for v in vertices {
			ret.push(Vertex { position: v, normal: normal.as_vec3() });
		}
	}

	ret
}
