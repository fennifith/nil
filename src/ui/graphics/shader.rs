use wgpu::*;
use glam::*;

use super::state::*;
use super::buffer::*;
use super::buffer_vertex::*;
use super::texture::*;
use super::util::*;
use crate::events::WorldOut;
use crate::constants::CHUNK_SIZE;
use super::build_cube;

pub const BUF_VERTICES: u32 = 0;
pub const BUF_INSTANCES: u32 = 1;

pub const BIND_CAMERA: u32 = 0;
pub const BIND_LIGHTING: u32 = 1;

// max instance num = CHUNK_SIZE^3
pub const BUF_INSTANCES_SIZE: BufferAddress = (CHUNK_SIZE.pow(3) * 16u32.pow(3)) as BufferAddress;

pub struct Shader {
	depth_texture: GameTexture,
	render_pipeline: RenderPipeline,

	camera_buffer: Buffer,
	camera_bind_group: BindGroup,

	lighting_buffer: Buffer,
	lighting_bind_group: BindGroup,

	vertex_buffer: VertexBuffer,
	instance_buffer: InstanceBuffer,
}

impl Shader {
	pub fn new(state: &State) -> Self {
		let (camera_buffer, camera_bind_layout, camera_bind_group) = create_uniform_buffer(
			&state.device,
			&state.camera.to_uniform(),
			ShaderStages::VERTEX,
			"camera"
		);

		let (lighting_buffer, lighting_bind_layout, lighting_bind_group) = create_uniform_buffer(
			&state.device,
			&state.lighting,
			ShaderStages::VERTEX | ShaderStages::FRAGMENT,
			"lighting"
		);

		let shader = state.device.create_shader_module(include_wgsl!("shader.wgsl"));

		let depth_texture = GameTexture::create_depth_texture(&state.device, &state.config, "depth_texture");

		let render_pipeline_layout = state.device.create_pipeline_layout(&PipelineLayoutDescriptor {
			label: Some("Render Pipeline Layout"),
			bind_group_layouts: &[
				&camera_bind_layout,
				&lighting_bind_layout,
			],
			push_constant_ranges: &[],
		});

		let render_pipeline = state.device.create_render_pipeline(&RenderPipelineDescriptor {
			label: Some("Render Pipeline"),
			layout: Some(&render_pipeline_layout),
			vertex: VertexState {
				module: &shader,
				entry_point: "vs_main",
				buffers: &[
					Vertex::buffer_layout(),
					super::Instance::buffer_layout(),
				],
			},
			fragment: Some(FragmentState {
				module: &shader,
				entry_point: "fs_main",
				targets: &[Some(ColorTargetState {
					format: state.config.format,
					blend: Some(BlendState::ALPHA_BLENDING),
					write_mask: ColorWrites::ALL,
				})],
			}),
			primitive: PrimitiveState {
				topology: PrimitiveTopology::TriangleList,
				strip_index_format: None,
				front_face: FrontFace::Ccw,
				cull_mode: Some(Face::Back),
				// Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
				polygon_mode: PolygonMode::Fill,
				// Requires Features::DEPTH_CLIP_CONTROL
				unclipped_depth: false,
				// Requires Features::CONSERVATIVE_RASTERIZATION
				conservative: false,
			},
			depth_stencil: Some(DepthStencilState {
				format: GameTexture::DEPTH_FORMAT,
				depth_write_enabled: true,
				depth_compare: CompareFunction::Less,
				stencil: StencilState::default(),
				bias: DepthBiasState::default(),
			}),
			multisample: MultisampleState {
				count: 1,
				mask: !0,
				alpha_to_coverage_enabled: true,
			},
			multiview: None,
		});

		let mut vertex_buffer = VertexBuffer::new("vertex_buffer", &state.device, 36);
		vertex_buffer.write(&state.queue, &build_cube()).unwrap();

		let instance_buffer = InstanceBuffer::new("instance_buffer", &state.device, BUF_INSTANCES_SIZE);

		Self {
			depth_texture,
			render_pipeline,
			camera_buffer,
			camera_bind_group,
			lighting_buffer,
			lighting_bind_group,
			vertex_buffer,
			instance_buffer,
		}
	}

	pub fn resize(&mut self, state: &State) {
		// resize depth texture
		self.depth_texture = GameTexture::create_depth_texture(&state.device, &state.config, "depth_texture");
	}

	pub fn update(&mut self, state: &mut State, events: &Vec<WorldOut>) {
		// update instance buffers
		for event in events {
			match event {
				WorldOut::UpdateChunk { chunk } => {
					// collect all instances into a vec
					let instances = chunk.voxel_map.iter()
						.map(|(_, voxel)| super::buffer_instance::Instance::new(voxel.pos().as_vec3(), voxel.material))
						.collect::<Vec<_>>();

					// schedule write to instance buffer
					self.instance_buffer.write(&state.queue, chunk.chunk.pos(), &instances).unwrap();
				},
				WorldOut::UnloadChunk { chunk_pos } => {
					self.instance_buffer.remove(*chunk_pos);
				},
				WorldOut::UpdatePlayer { player } => {
					state.camera.pos = player.pos;
				},
			}
		}

		// write to camera uniform buffer
		state.queue.write_buffer(&self.camera_buffer, 0, bytemuck::cast_slice(&[state.camera.to_uniform()]));
	}

	pub fn render(&mut self, state: &mut State) -> Result<(), SurfaceError> {
		let output = state.surface.get_current_texture()?;
		let view = output.texture.create_view(&TextureViewDescriptor::default());

		let mut encoder = state.device.create_command_encoder(&CommandEncoderDescriptor {
			label: Some("Render Encoder"),
		});

		{
			let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
				label: Some("Render Pass"),
				color_attachments: &[Some(RenderPassColorAttachment {
					view: &view,
					resolve_target: None,
					ops: Operations {
						load: LoadOp::Clear(Color::BLACK),
						store: true,
					},
				})],
				depth_stencil_attachment: Some(RenderPassDepthStencilAttachment {
					view: &self.depth_texture.view,
					depth_ops: Some(Operations {
						load: LoadOp::Clear(1.0),
						store: true,
					}),
					stencil_ops: None,
				}),
			});

			render_pass.set_pipeline(&self.render_pipeline);

			// TODO: any reason *not* to centralize these values in a HashMap<u32, BindGroup>?
			render_pass.set_bind_group(BIND_CAMERA, &self.camera_bind_group, &[]);
			render_pass.set_bind_group(BIND_LIGHTING, &self.lighting_bind_group, &[]);

			render_pass.set_vertex_buffer(BUF_VERTICES, self.vertex_buffer.buffer.slice(..));
			render_pass.set_vertex_buffer(BUF_INSTANCES, self.instance_buffer.buffer.slice(..));

			// draw each instance buffer (per chunk)
			for chunk in self.instance_buffer.allocator.blocks() {
				render_pass.draw(0..36, chunk.range());
			}
		}

		state.queue.submit(std::iter::once(encoder.finish()));
		output.present();

		Ok(())
	}
}
