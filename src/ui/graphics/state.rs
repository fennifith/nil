use std::sync::Arc;

use anyhow::Result;
use crossbeam_channel::{Sender, Receiver};
use winit::{
	event::*,
	event_loop::{ControlFlow, EventLoop},
	window::{Window, WindowBuilder, Fullscreen},
};
use wgpu::*;

use crate::{ui::{
	graphics::lighting::*,
	camera::Camera,
	input::Input,
}, events::*};
use crate::game::GameState;

use super::shader::*;

pub struct State {
	pub surface: Surface,
	pub device: Device,
	pub queue: Queue,
	pub config: SurfaceConfiguration,
	pub size: winit::dpi::PhysicalSize<u32>,

	pub camera: Camera,
	pub lighting: Lighting,
	pub input: Input,
}

impl State {
	async fn new(window: &Window) -> Result<Self> {
		let size = window.inner_size();

		// The instance is a handle to our GPU
		// BackendBit::PRIMARY => Vulkan + Metal + DX12 + Browser WebGPU
		let instance = Instance::new(InstanceDescriptor::default());
		let surface = unsafe { instance.create_surface(window) }?;
		let adapter = instance.request_adapter(&RequestAdapterOptions {
			power_preference: PowerPreference::default(),
			compatible_surface: Some(&surface),
			force_fallback_adapter: false,
		}).await.unwrap();

		let (device, queue) = adapter.request_device(
			&DeviceDescriptor {
				label: None,
				features: Features::empty(),
				limits: Limits::default(),
			},
			// Some(&std::path::Path::new("trace")), // Trace path
			None,
		).await.unwrap();

		let config = surface.get_default_config(&adapter, size.width, size.height).unwrap();

		// let config = SurfaceConfiguration {
		// 	usage: TextureUsages::RENDER_ATTACHMENT,
		// 	format: surface.get_preferred_format(&adapter).unwrap(),
		// 	width: size.width,
		// 	height: size.height,
		// 	present_mode: PresentMode::Fifo,
		// };
		surface.configure(&device, &config);

		let camera = Camera::new();
		let lighting = Lighting::default();
		let input = Input::new();

		Ok(Self {
			surface,
			device,
			queue,
			config,
			size,

			camera,
			lighting,
			input,
		})
	}

	pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
		if new_size.width > 0 && new_size.height > 0 {
			self.size = new_size;
			self.config.width = new_size.width;
			self.config.height = new_size.height;
			self.surface.configure(&self.device, &self.config);

			// resize the camera perspective
			self.camera.resize(new_size.width, new_size.height);
		}
	}

	fn update(&mut self) {
		// let (dx, dy) = self.input.get_mouse_delta();
		// self.camera.update_mouse(dx, dy);

		// TODO: update user position?
	}
}

pub fn run(game: Arc<GameState>, world_in_chan: Sender<WorldIn>, world_out_chan: Receiver<WorldOut>) {
	let rt = tokio::runtime::Builder::new_current_thread().build().unwrap();

	let event_loop = EventLoop::new();
	let window = WindowBuilder::new()
		.with_fullscreen(Some(Fullscreen::Borderless(None)))
		.build(&event_loop)
		.unwrap();

	// State::new uses async code, so we're going to wait for it to finish
	let mut state = rt.block_on(State::new(&window)).unwrap();
	let mut shader = Shader::new(&state);

	event_loop.run(move |event, _, control_flow| {
		match event {
			Event::DeviceEvent {
				ref event,
				.. // We're not using device_id currently
			} => {
				match event {
					DeviceEvent::MouseMotion { delta } => {
						state.camera.update_mouse(delta.0, delta.1);
					},
					e => {
						state.input.handle_event(e);
					}
				}

				if state.input.is_invalid {
					world_in_chan.send(WorldIn::PlayerInput {
						uuid: game.player_id,
						input: state.input.get_state(&state.camera),
					}).ok();

					state.input.is_invalid = false;
				}
			},
			Event::WindowEvent {
				ref event,
				window_id,
			} if window_id == window.id() => match event {
				WindowEvent::CloseRequested | WindowEvent::KeyboardInput {
					input: KeyboardInput {
						state: ElementState::Pressed,
						virtual_keycode: Some(VirtualKeyCode::Escape),
						..
					}, ..
				} => *control_flow = ControlFlow::Exit,
				WindowEvent::Resized(physical_size) => {
					state.resize(*physical_size);
					shader.resize(&state);
				},
				WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
					state.resize(**new_inner_size);
					shader.resize(&state);
				},
				_ => {},
			},
			Event::RedrawRequested(window_id) if window_id == window.id() => {
				// update input/camera states
				let mut events: Vec<WorldOut> = vec![];
				while let Ok(e) = world_out_chan.try_recv() {
					events.push(e);
				}

				shader.update(&mut state, &events);

				// render the sketchy model-instanced cube shader
				let ret = shader.render(&mut state);

				match ret {
					Ok(_) => {}
					// Reconfigure the surface if lost
					Err(SurfaceError::Lost) => state.resize(state.size),
					// The system is out of memory, we should probably quit
					Err(SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
					// All other errors (Outdated, Timeout) should be resolved by the next frame
					Err(e) => eprintln!("{:?}", e),
				}
			}
			Event::MainEventsCleared => {
				// RedrawRequested will only trigger once, unless we manually
				// request it.
				window.request_redraw();
			}
			_ => {}
		}
	});
}
