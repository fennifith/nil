use wgpu::*;

pub struct GameTexture {
	pub texture: Texture,
	pub view: TextureView,
	pub sampler: Sampler,
}

impl GameTexture {
	pub const DEPTH_FORMAT: TextureFormat = TextureFormat::Depth32Float;

	pub fn create_depth_texture(device: &Device, config: &SurfaceConfiguration, label: &str) -> Self {
		let size = Extent3d {
			width: config.width,
			height: config.height,
			depth_or_array_layers: 1,
		};
		let desc = TextureDescriptor {
			label: Some(label),
			size,
			mip_level_count: 1,
			sample_count: 1,
			dimension: TextureDimension::D2,
			format: Self::DEPTH_FORMAT,
			usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
			view_formats: &[TextureFormat::Depth32Float],
		};
		let texture = device.create_texture(&desc);

		let view = texture.create_view(&TextureViewDescriptor::default());
		let sampler = device.create_sampler(
			&SamplerDescriptor {
				address_mode_u: AddressMode::ClampToEdge,
				address_mode_v: AddressMode::ClampToEdge,
				address_mode_w: AddressMode::ClampToEdge,
				mag_filter: FilterMode::Linear,
				min_filter: FilterMode::Linear,
				mipmap_filter: FilterMode::Nearest,
				compare: Some(CompareFunction::LessEqual),
				lod_min_clamp: 0.0,
				lod_max_clamp: 100.0,
				..Default::default()
			}
		);

		Self { texture, view, sampler }
	}
}
