use glam::*;
use crate::{blocks::{LoadedChunk, Player}, ui::InputState};

#[derive(Debug)]
pub enum WorldIn {
	PlayerInput { uuid: uuid::Uuid, input: InputState },
	LoadChunk { chunk: LoadedChunk },
}

#[derive(Debug)]
pub enum WorldOut {
	UpdatePlayer { player: Player },
	UpdateChunk { chunk: LoadedChunk },
	UnloadChunk { chunk_pos: IVec3 },
}
