.PHONY: dev
.DEFAULT_GOAL := dev

dev:
	cargo run

fps:
	GALLIUM_HUD=fps VK_INSTANCE_LAYERS=VK_LAYER_MESA_overlay cargo run

db:
	docker compose up -d db
	sqlx migrate run

db-clean:
	docker compose down
	docker volume prune -f

clean: db-clean
	rm -rf target/
	rm -f Cargo.lock
