# NIL

After installing [Rust](https://www.rust-lang.org), the project can be started with either `make` or `cargo run`.

Using `make fps` should display an FPS indicator, which can be useful for performance monitoring.
